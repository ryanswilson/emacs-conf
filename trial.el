(use-package exec-path-from-shell
  :ensure
  :init (exec-path-from-shell-initialize))

;; (use-package dap-mode
;;   :ensure
;;   :config
;;   (dap-ui-mode)
;;   (dap-ui-controls-mode 1)

;;   (require 'dap-lldb)
;;   (require 'dap-gdb-lldb)
;;   ;; installs .extension/vscode
;;   (dap-gdb-lldb-setup)
;;   (dap-register-debug-template
;;    "Rust::LLDB Run Configuration"
;;    (list :type "lldb"
;;          :request "launch"
;;          :name "LLDB::Run"
;; 	 :gdbpath "rust-lldb"
;;          :target nil
;;          :cwd nil)))

(setq dap-cpptools-extension-version "1.5.1")

(with-eval-after-load 'lsp-rust
  (require 'dap-cpptools))

(with-eval-after-load 'dap-mode
  (setq dap-default-terminal-kind "integrated") ;; Make sure that terminal programs open a term for I/O in an Emacs buffer
  (dap-auto-configure-mode +1))

(with-eval-after-load 'dap-cpptools
  ;; Add a template specific for debugging Rust programs.
  ;; It is used for new projects, where I can M-x dap-edit-debug-template
  (dap-register-debug-template
   "Rust::CppTools Run Configuration"
   (list :type "cppdbg"
         :request "launch"
         :name "Rust::Run"
         :MIMode "gdb"
         :miDebuggerPath "rust-gdb"
         :environment []
         :program "${workspaceFolder}/target/debug/stolif"
         :cwd "${workspaceFolder}"
         :console "external"
         :dap-compilation "cargo build"
         :dap-compilation-dir "${workspaceFolder}")))


(dap-mode 1)
(dap-ui-mode)
(dap-ui-controls-mode 0)
(require 'dap-gdb-lldb)

(dap-register-debug-template "Rust::GDB Run Configuration"
                             (list :type "gdb"
                                   :request "launch"
                                   :name "GDB::Run"
				   :MIMode "gdb"
				   :miDebuggerPath "rust-gdb"
                                   :target nil
                                   :cwd nil))



(define-key c-mode-map (kbd "C-.") 'lsp-find-definition)
(define-key c++-mode-map (kbd "C-.") 'lsp-find-definition)

(require 'calfw)

(require 'calfw-ical)
;; do something with gcal https://github.com/myuhe/calfw-gcal.el/blob/master/calfw-gcal.el


(defun tabbar-line-format (tabset)
  "Return the `header-line-format' value to display TABSET."
  (let* ((sel (tabbar-selected-tab tabset))
         (tabs (tabbar-view tabset))
         (padcolor (tabbar-background-color))
         atsel elts)
    ;; Initialize buttons and separator values.
    (or tabbar-separator-value
        (tabbar-line-separator))
    (or tabbar-home-button-value
        (tabbar-line-button 'home))
    (or tabbar-scroll-left-button-value
        (tabbar-line-button 'scroll-left))
    (or tabbar-scroll-right-button-value
        (tabbar-line-button 'scroll-right))
    ;; Track the selected tab to ensure it is always visible.
    (when tabbar--track-selected
      (while (not (memq sel tabs))
        (tabbar-scroll tabset -1)
        (setq tabs (tabbar-view tabset)))
      (while (and tabs (not atsel))
        (setq elts  (cons (tabbar-line-tab (car tabs)) elts)
              atsel (eq (car tabs) sel)
              tabs  (cdr tabs)))
      (if tabs
          (setq elts  (cons (tabbar-line-tab (car tabs)) elts)
              atsel (eq (car tabs) sel)
              tabs  (cdr tabs)))
      (setq elts (nreverse elts))
      ;; At this point the selected tab is the last elt in ELTS.
      ;; Scroll TABSET and ELTS until the selected tab becomes
      ;; visible.
      (with-temp-buffer
        (let ((truncate-partial-width-windows nil)
              (inhibit-modification-hooks t)
              deactivate-mark ;; Prevent deactivation of the mark!
              start)
          (setq truncate-lines nil
                buffer-undo-list t)
          (apply 'insert (tabbar-line-buttons tabset))
          (setq start (point))
          (while (and (cdr elts) ;; Always show the selected tab!
                      (progn
                        (delete-region start (point-max))
                        (goto-char (point-max))
                        (apply 'insert elts)
                        (goto-char (point-min))
                        (> (vertical-motion 1) 0)))
            (tabbar-scroll tabset 1)
            (setq elts (cdr elts)))))
      (setq elts (nreverse elts))
      (setq tabbar--track-selected nil))
    ;; Format remaining tabs.
    (while tabs
      (setq elts (cons (tabbar-line-tab (car tabs)) elts)
            tabs (cdr tabs)))
    ;; Cache and return the new tab bar.
    (tabbar-set-template
     tabset
     (list (tabbar-line-buttons tabset)
           (nreverse elts)
           (propertize "%-"
                       'face (list :background padcolor
                                   :foreground padcolor)
                       'pointer 'arrow)))
    ))


;; (global-set-key (kbd "C-t") 'origami-toggle-node)

;; (pyvenv-activate "~/everlaw/servers/.env")
(require 'dap-python)
(setq dap-python-debugger 'debugpy)
;; (pyvenv-activate "/home/ryan/Desktop/code/nanoGPT/venv")

(define-key dap-mode-map (kbd "H-s") 'dap-switch-stack-frame)

(dap-register-debug-template
 "Python cust first stage"
 (list :type "python"
       :args "/home/ryanwilson/everlaw/servers/api/everlaw.yaml base"
       :cwd "/home/ryanwilson/everlaw/servers/api/scripts/build/"
       :env '(("DEBUG" . "1"))
       :target-module "/home/ryanwilson/everlaw/servers/api/scripts/build/generate_api.py"
       :request "launch"
       :name "My App"))

(dap-register-debug-template
 "Python cust second stage"
 (list :type "python"
       :args "/home/ryanwilson/everlaw/servers/api/everlaw.yaml /home/ryanwilson/everlaw/servers/api/logo.svg /tmp/wow.yaml"
       :cwd "/home/ryanwilson/everlaw/servers/api/scripts/build/"
       :env '(("DEBUG" . "1"))
       :target-module "/home/ryanwilson/everlaw/servers/api/scripts/build/translate_schema.py"
       :request "launch"
       :name "My App"))

(dap-register-debug-template "nano-gpt"
  (list :type "python"
        :args "config/train_shakespeare_char.py"
        :cwd nil
        :env '(("DEBUG" . "1"))
        :target-module (expand-file-name "~/Desktop/code/nanoGPT/train.py")
        :request "launch"
        :name "nano-gpt"))

(add-hook 'prog-mode-hook 'copilot-mode)
(include 'copilot)
(define-key copilot-completion-map (kbd "<tab>") 'copilot-accept-completion)
(define-key copilot-completion-map (kbd "TAB") 'copilot-accept-completion)
(global-set-key (kbd "H-.") 'copilot-complete)

(define-key copilot-completion-map (kbd "M-n") 'copilot-next-completion)
(define-key copilot-completion-map (kbd "M-p") 'copilot-previous-completion)
(define-key copilot-completion-map (kbd "M-f") 'copilot-accept-completion-by-line)

(define-key copilot-completion-map (kbd "C-n") nil)
(define-key copilot-completion-map (kbd "C-p") nil)
(define-key copilot-completion-map (kbd "M-g") 'copilot-clear-overlay)
