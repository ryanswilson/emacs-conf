(maybe-require-package 'editorconfig)
(editorconfig-mode 1)
(git-ensure-package "https://github.com/zerolfx/copilot.el" "copilot")
(require 'copilot)
(add-hook 'prog-mode-hook 'copilot-mode)


(define-key copilot-completion-map (kbd "<tab>") 'copilot-accept-completion)
(define-key copilot-completion-map (kbd "TAB") 'copilot-accept-completion)
(global-set-key (kbd "H-.") 'copilot-complete)
(global-set-key (kbd "s-.") 'copilot-complete)

(define-key copilot-completion-map (kbd "M-n") 'copilot-next-completion)
(define-key copilot-completion-map (kbd "M-p") 'copilot-previous-completion)
(define-key copilot-completion-map (kbd "M-f") 'copilot-accept-completion-by-line)
(define-key copilot-completion-map (kbd "M-l") 'copilot-accept-completion-by-word)

(global-set-key (kbd "C-M-g") 'copilot-mode)

(provide 'init-copilot)
