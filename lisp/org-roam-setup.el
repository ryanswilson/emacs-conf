;; -*- lexical-binding: t -*-

;;--------------------------------------------------------------------------------
;; This config is largely ripped from system crafters tutorials on org roam
;;--------------------------------------------------------------------------------

(defun org-roam-node-insert-immediate (arg &rest args)
  (interactive "P")
  (save-window-excursion
    (let ((args (cons arg args))
          (org-roam-capture-templates (list (append (car org-roam-capture-templates)
                                                    '(:immediate-finish t)))))
      (apply #'org-roam-node-insert args))))

(defun my/org-roam-filter-by-tag (tag-name)
  (lambda (node)
    (member tag-name (org-roam-node-tags node))))

(defun my/org-roam-filter-by-tag (tag-name)
  (lambda (node)
    (member tag-name (org-roam-node-tags node))))

(defun my/org-roam-list-notes-by-tag (tag-name)
  (mapcar #'org-roam-node-file
          (seq-filter
           (my/org-roam-filter-by-tag tag-name)
           (org-roam-node-list))))

(defun my/org-roam-refresh-agenda-list ()
  (interactive)
  (setq org-agenda-files (append
			  '("/home/ryanwilson/Documents/agenda.org")
			  (my/org-roam-list-notes-by-tag "Project"))))

;; (defun my/org-roam-refresh-agenda-list ()
;;   (interactive)
;;   (setq org-agenda-files (append
;; 			  '("/home/ryanwilson/Documents/agenda.org")
;; 			  (my/org-roam-list-notes-by-tag "Project")
;; 			  (my/org-roam-list-notes-by-tag "AProject"))))

(defun my/org-roam-project-finalize-hook ()
  "Adds the captured project file to `org-agenda-files' if the
capture was not aborted."
  ;; Remove the hook since it was added temporarily
  (remove-hook 'org-capture-after-finalize-hook #'my/org-roam-project-finalize-hook)

  ;; Add project file to the agenda list if the capture was confirmed
  (unless org-note-abort
    (with-current-buffer (org-capture-get :buffer)
      (add-to-list 'org-agenda-files (buffer-file-name)))))

(defun my/org-roam-find-project ()
  (interactive)
  ;; Add the project file to the agenda after capture is finished
  (add-hook 'org-capture-after-finalize-hook #'my/org-roam-project-finalize-hook)

  ;; Select a project file to open, creating it if necessary
  (org-roam-node-find
   nil
   nil
   (my/org-roam-filter-by-tag "Project")
     ;; :templates
   ;; '(("p" "project" plain "* Goals\n\n%?\n\n* Tasks\n\n** TODO Add initial tasks\n\n* Dates\n\n"
   ;;    :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+category: ${title}\n#+filetags: Project")
   ;;    :unnarrowed t))
   ))

(global-set-key (kbd "C-c n p") 'my/org-roam-find-project)

(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/Desktop/notes")
  (org-roam-completion-everywhere t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n I" . org-roam-node-insert-immediate)
	 ("C-c n i" . org-roam-node-insert)
	 ("C-c n a" . org-roam-alias-add)
	 ("C-c n A" . my/org-roam-refresh-agenda-list)
	 ("C-c n r" . org-roam-db-sync)
         :map org-mode-map
         ("C-M-i" . completion-at-point)
	 ("C-c n d" . org-id-get-create)
         :map org-roam-dailies-map
         ("Y" . org-roam-dailies-capture-yesterday)
         ("T" . org-roam-dailies-capture-tomorrow))
  :bind-keymap
  ("C-c n d" . org-roam-dailies-map)
  :config
  (require 'org-roam-dailies)
  (org-roam-db-autosync-mode)
  ;; Build the agenda list the first time for the session
  (my/org-roam-refresh-agenda-list))

(setq org-roam-capture-templates
      '(("d" "default" plain
	 "%?"
	 :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
	 :unnarrowed t)
	("p" "project" plain "* Goals\n\n%?\n\n* Tasks\n\n** TODO Add initial tasks\n\n* Dates\n\n"
 :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+TODO: TODO BLOCKED | DONE\n#+category: ${title}\n#+filetags: Project")
 :unnarrowed t)))


(provide 'org-roam-setup)
