;;--------------------------------------------------------------------------------
;; setting this so I can keep nonstandard configs for myself
;;--------------------------------------------------------------------------------

(defvar is-ravar)
(setq is-ravar nil)

;;--------------------------------------------------------------------------------
;; handy function for quick dictionary lookup. However since I doubt you will be learning
;; danish anytime soon this function is here mostly to give you an idea how you use it
;; I have custom logic because I am using a local dictionary instead of the dict.org
;; dictionary
;;--------------------------------------------------------------------------------

;;example usage
;;(setq dictionary-server "dict.org")
;;(dictionary-search "sten" "fd-dan-eng")

(if is-ravar (setq dictionary-server "localhost"))

(defun danish-dictionary-at-point ()
	"uses the dictionary package from melpa to search word at point in the fd-dan-eng dictionary"
	(interactive)
	(if is-ravar
	    (dictionary-search (word-at-point) "dan-eng" )
	  (dictionary-searc (word-at-point "fd-dan-eng"))))

;;--------------------------------------------------------------------------------
;; this allow for adjusting the size of latex fragments
;; in tandem with the font size. 
;;--------------------------------------------------------------------------------
(defun my-buffer-scale ()
  "returns the text scale float of the current buffer"
  (interactive)
  (expt text-scale-mode-step text-scale-mode-amount))

(defun org-format-latex--advice (fun &rest r)
  (let (
	(oscale (plist-get org-format-latex-options :scale))
	(mult (my-buffer-scale)))
    (progn
      (plist-put org-format-latex-options :scale
		 (* mult oscale))
      (apply fun r)
      (plist-put org-format-latex-options :scale oscale))))

(add-function :around (symbol-function 'org-format-latex) #'org-format-latex--advice)

;;--------------------------------------------------------------------------------
;; This allows for easy cross linking of .org files used to keep research notes
;;--------------------------------------------------------------------------------
(defun rorg-double-link ()
  "yeah"
  (interactive)
  
  (let ((ref-fn nil)
	(orig-buffer (current-buffer))
	(orig-fn (buffer-file-name (current-buffer)))
	(orig-ln (line-number-at-pos)))
    (save-excursion
      (helm-find-files-1 "~/Desktop/research-orgs/concepts/")
      (setq ref-fn (buffer-file-name (current-buffer)))
      (goto-char (point-max))
      (if (eq 0 (funcall outline-level))
	  (progn
	    (org-insert-heading)
	    (insert "Referenced By\n"))
	  (if (not (string= (nth 4 (org-heading-components)) "Referenced By"))
	      (progn
		(org-insert-heading)
		(insert "Referenced By\n"))))
      (insert (concat "[[file:" orig-fn "::" (number-to-string orig-ln)
		      "][" (rorg-abbrev orig-fn) "]]\n" ))
      (save-buffer)
      (kill-buffer))
    (switch-to-buffer orig-buffer)
    (insert (concat "[[file:" ref-fn "][" (rorg-abbrev ref-fn) "]]" ))))

(defun rorg-abbrev (fn)
  "trims file name"
  (let ((abbrev (substring fn (string-match "[a-zA-Z-_]+.[a-zA-Z]+$" fn) nil)))
    (substring abbrev 0 (string-match ".[a-zA-Z]+$" abbrev))))

;;--------------------------------------------------------------------------------
;; silences flyspell for words that are all caps or are camel case
;;--------------------------------------------------------------------------------
(defun flyspell-ignore-abbrev () (interactive)
       (save-excursion
	 (forward-whitespace -1)
	 (when (looking-at "[ \n]")
	   (forward-char))
	 (not (let ((case-fold-search nil))
		(looking-at "[A-Z]+")))))

(put 'org-mode 'flyspell-mode-predicate 'flyspell-ignore-abbrev)
(put 'text-mode 'flyspell-mode-predicate 'flyspell-ignore-abbrev)
(put 'markdown-mode 'flyspell-mode-predicate 'flyspell-ignore-abbrev)

(defun flyspell-ignore-camel () (interactive)
       (save-excursion
	 (forward-whitespace -1)
	 (when (looking-at "[ \n]")
	   (forward-char))
	 (not (let ((case-fold-search nil))
		(looking-at "[A-Z][a-z]*[A-Z][a-Z]*")))))

(put 'org-mode 'flyspell-mode-predicate 'flyspell-ignore-camel)
(put 'text-mode 'flyspell-mode-predicate 'flyspell-ignore-camel)
(put 'markdown-mode 'flyspell-mode-predicate 'flyspell-ignore-camel)

;;--------------------------------------------------------------------------------
;; prevent auctex from removing focus from active tex buffer after save
;;--------------------------------------------------------------------------------

(defun frame-return--advice (fun &res r)
  (let (frame (selected-frame))
    (progn
      (apply fun r)
      (select-frame-set-input-focus frame))))

;; TODO fix this, doesn't run correctly
(with-eval-after-load "latex"
  (add-function :around (symbol-function 'TeX-region-update-point)
		#'frame-return--advice)
  (add-function :around (symbol-function 'TeX-region-update)
		#'frame-return--advice))

;;--------------------------------------------------------------------------------
;; not really used, this is an attempt to make navigating latex nicer
;;--------------------------------------------------------------------------------
(defun avy-jump-open ()
  (interactive)
  (avy--generic-jump "{" nil)
  (forward-char))

;;--------------------------------------------------------------------------------
;; look at http://www.mycpu.org/emacs-productivity-setup/ ??
;; pulled from https://news.ycombinator.com/item?id=22129636
;;--------------------------------------------------------------------------------

(defun arrayify (start end quote)
    "Turn strings on newlines into a QUOTEd, comma-separated one-liner."
    (interactive "r\nMQuote: ")
    (let ((insertion
           (mapconcatppp
            (lambda (x) (format "%s%s%s" quote x quote))
            (split-string (buffer-substring start end)) ", ")))
      (delete-region start end)
      (insert insertion)))

;;--------------------------------------------------------------------------------
;; allows yanking lines across frames while not chaning location of point
;;--------------------------------------------------------------------------------
(defun avy-yank-line (u)
  (interactive "P")
  (let ((avy-all-windows 'all-frames)
	(start-frame (selected-frame)))
    (save-excursion (avy--generic-jump "\n" nil)
		    (move-beginning-of-line nil)
		    (set-mark-command nil)
		    (move-end-of-line nil)
		    (if u (kill-region 0 1 1) (kill-ring-save 0 1 1)))
    (select-frame-set-input-focus start-frame))) 

;;--------------------------------------------------------------------------------
;; macro for cleaning up lisp spacing 
;;--------------------------------------------------------------------------------
(fset 'collapse-head-whitespace
      [?\C-  ?\C-a backspace backspace])

;;turns 
;;( progn
;;     (some expression))
;;into
;;( progn (some expression))

;;--------------------------------------------------------------------------------
;; macro to copy the for a word pinyin out of youdao 
;;--------------------------------------------------------------------------------
(fset 'youdao-extract
      (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([5 2 67108896 5 134217744 6 134217847] 0 "%d")) arg)))

;;--------------------------------------------------------------------------------
;; trying to emulate luke smith functionality
;;--------------------------------------------------------------------------------
(defun move-temp-del ()
  (interactive)
  (progn (search-forward "++")
	 (delete-char -2)))

;;--------------------------------------------------------------------------------
;; used in a yasnippet snippet to speed up typing of chemical formulas
;;--------------------------------------------------------------------------------
(defun chem-convert (text)
  (replace-regexp-in-string "_?[0-9]+$"
      (lambda (x) (format "_%s" (replace-regexp-in-string "_" "" x)))
      text))

;;--------------------------------------------------------------------------------
;; docstring says it all
;;--------------------------------------------------------------------------------
(setq ravar/elfeed-podcast-dir "~/Desktop/music/podcast/")
;;TODO check mimetype first?
(defun ravar/elfeed-play-enclosure-mpd ()
  "Downloads the item in the enclosure and starts in playing in mpd using mpc"
  (interactive)
  (let* ((entry elfeed-show-entry)
	 (enclosure-index (elfeed--get-enclosure-num
			   "Enclosure to save" entry))
         (url-enclosure (car (elt (elfeed-entry-enclosures entry)
                                  (- enclosure-index 1))))
	 (fname
          (funcall elfeed-show-enclosure-filename-function
                   entry url-enclosure)))
    (start-process-shell-command
     "play enclosure" nil
     (format "cd %s; wget %s;mpc update; mpc search filename %s | mpc insert; 
mpc next; mpc play "
	     ravar/elfeed-podcast-dir url-enclosure fname fname))))

;;--------------------------------------------------------------------------------
;; used to allow quick saving of images from q4
;;--------------------------------------------------------------------------------
(defun q4-override/wget-image (addr)
  "This will be attached to the mouse action for a button 
and will be used to save the associated image"
  (interactive)
  (start-process-shell-command
   "q4-save-im" nil (format "cd ~/Pictures/4chan; wget %s " addr)))

(defun q4-override/save-post-image ()
  "save the posts current image in specified directory"
  (interactive)
  (save-excursion
    (q4/assert-post-start)
    (let ((image (q4/next-prop 'image nil (q4/sep-pos))))
      (if image (push-button image t)
        (message "No image in this post.")))))

;;--------------------------------------------------------------------------------
;; surprisingly useful missing functionality
;;--------------------------------------------------------------------------------
(defun revert-this-buffer ()
  (interactive)
  (revert-buffer nil t t)
  (message (concat "Reverted buffer " (buffer-name))))


;;--------------------------------------------------------------------------------
;; aesthetic centering for full screen reading
;;--------------------------------------------------------------------------------
(defun nov-center-text-after-render ()
  (interactive)
  "add to nov-post-html-render-hook to make the text centered in the frame"
  (cond
   (nov-text-width
    (let* ((width (window-width))
           (offset (- (truncate (* (- width nov-text-width) .5)) 6))
           (prepend (make-string offset ? )))
      (string-insert-rectangle (point-min) (point-max) prepend)))))

(defun nov-save ()
  (interactive)
  "saves the current position"
  (let ((identifier (cdr (assq 'identifier nov-metadata)))
          (index (if (integerp nov-documents-index)
                     nov-documents-index
                   0)))
    (nov-save-place identifier index (point))))

;; this allows nov to correctly render rubies, aka japanese pronunciation hints
(defun shr-tag-rt (dom)
  (let ((start (point)))
    (shr-generic dom)
    ;; (put-text-property start (point) 'display '(height .8))
    (put-text-property start (point) 'display '(raise 0.4))
    (add-face-text-property start (point) '(:height .8))))

;; (defun my-nov-seg-japanese ()
;;   (interactive)
;;   (let* ((html (buffer-substring-no-properties
;; 		(point-min) (point-max)))
;; 	 (new-html
;; 	  (with-temp-buffer
;; 	    (call-process (append "/home/ryan/Desktop/code/jade/jcli" "$'" html "'") nil t )
;; 	    (buffer-substring-no-properties
;; 	     (point-min) (point-max)))))
;;     (goto-char (point-min))
;;     (insert new-html)
;;     (delete-region (point) (point-max))))

;;(add-hook 'nov-pre-html-render-hook 'my-nov-seg-japanese)
(defun my-nov-seg-japanese ()
  (interactive)
  (call-process-region (point-min) (point-max)
		       "/home/ryan/Desktop/code/jade/jcli"
		       t
		       t))

;;--------------------------------------------------------------------------------
;; This makes company completion cooperate with yasnippet, minor mode keybind in init.el
;;--------------------------------------------------------------------------------

(defun check-expansion ()
  (save-excursion
    (if (looking-at "\\_>") t
      (backward-char 1)
      (if (looking-at "\\.") t
        (backward-char 1)
        (if (looking-at "->") t nil)))))

(defun tab-indent-or-complete ()
  (interactive)
  (if (minibufferp)
      (minibuffer-complete)
    (if (or (not yas/minor-mode)
            (null (do-yas-expand)))
        (if (check-expansion)
            (company-complete-common)
          (indent-for-tab-command)))))

(defun ravar-tab-or-normal ()
  (interactive)
  (if (minibufferp)
      (minibuffer-complete)
    (if (or (not yas/minor-mode)
            (null (do-yas-expand)))
	(indent-for-tab-command))))

(defun do-yas-expand ()
  (let ((yas-fallback-behavior 'return-nil))
    (yas-expand)))

(eval-after-load "company"
  '(define-key company-active-map (kbd "<tab>") 'ravar-tab-or-normal))

;;--------------------------------------------------------------------------------
;; titles usually self explanatory, not sure what i used them for
;;--------------------------------------------------------------------------------
(fset 'copy-next-line
      (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([14 1 67108896 5 134217847] 0 "%d")) arg)))

(defun my-reverse-region (beg end)
 "Reverse characters between BEG and END."
 (interactive "r")
 (let ((region (buffer-substring beg end)))
   (delete-region beg end)
   (insert (nreverse region))))

(defun add-to-kill (n-str blank) (progn
				   (kill-new n-str )
				   (message "copied: %S" n-str)))

(defun get-buffers-matching-mode (mode)
  "Returns a list of buffers where their major-mode is equal to MODE"
  (let ((buffer-mode-matches '()))
   (dolist (buf (buffer-list))
     (with-current-buffer buf
       (if (eq mode major-mode)
           (add-to-list 'buffer-mode-matches buf))))
   buffer-mode-matches))

(defun multi-occur-in-this-mode ()
  "Show all lines matching REGEXP in buffers with this major mode."
  (interactive)
  (multi-occur
   (get-buffers-matching-mode major-mode)
   (car (occur-read-primary-args))))

(defun clopen-buffer ()
  "derp"
  (interactive)
  (let ((val buffer-file-name))
    (progn (kill-buffer nil)
	   (switch-to-buffer (find-file-noselect val)))))

;;--------------------------------------------------------------------------------
;; used to open xref in buffer to be determined
;;--------------------------------------------------------------------------------

(defun rsw-xgo-buffer (buffer)
  (interactive (list (read-buffer "Pop to buffer: " (other-buffer))
		     (if current-prefix-arg t)))
  (let* ((old-frame (selected-frame))
	     (window (aw-select " xgo")))
    ;; Don't assume that `display-buffer' has supplied us with a window
    ;; (Bug#24332).

    (let ((frame (window-frame window)))
          ;; If we chose another frame, make sure it gets input focus.
          (unless (eq frame old-frame)
            (select-frame-set-input-focus frame))
          ;; Make sure the window is selected (Bug#8615), (Bug#6954)
          (select-window window))
    (switch-to-buffer buffer))
  buffer)

(defun xgo-return-window (buffer &optional action)
  "Show temporary buffer BUFFER in a window.
Return the window showing BUFFER.  Pass ACTION as action argument
to `display-buffer'."
  (interactive (list (read-buffer "Pop to buffer: " (other-buffer))
		     (if current-prefix-arg t)))
  (let* ((old-frame (selected-frame))
	 (window (aw-select " xgo")))
    ;; Don't assume that `display-buffer' has supplied us with a window
    ;; (Bug#24332).

    (let ((frame (window-frame window)))
      ;; If we chose another frame, make sure it gets input focus.
      (unless (eq frame old-frame)
        (select-frame-set-input-focus frame))
      ;; Make sure the window is selected (Bug#8615), (Bug#6954)
      (select-window window))
    (switch-to-buffer buffer)
    window))

(defun xgo-no-op (a b)
  "do nothing function"
  (message "wow"))

(setq xgo-switch-override-alist '(("Org todo" switch-to-buffer)))

(defun buffer-or-string-to-string (var)
  "Return the name of the buffer if VAR is a buffer, otherwise return VAR as a string."
  (if (bufferp var)
      (buffer-name var)
    (prin1-to-string var)))

(defun switch-to-buffer-other-window (buffer-or-name &optional norecord)
  "Switch to BUFFER-OR-NAME in another window, using override if regex matches."
  (interactive
   (list (read-buffer-to-switch "Switch to buffer in other window: ")))
  (let ((buffer-name (if (bufferp buffer-or-name)
                         (buffer-name buffer-or-name)
                       buffer-or-name)))
    (if-let ((override-fn
	      (car (xgo-display-buffer-assq-regexp buffer-name xgo-switch-override-alist))))
        (apply override-fn buffer-or-name norecord)
      (let* ((buffer (window-normalize-buffer-to-switch-to buffer-or-name))
             (old-frame (selected-frame))
             (window (xgo-return-window buffer norecord)))
        ;; Don't assume that `display-buffer' has supplied us with a window
        ;; (Bug#24332).
        (if window
            (let ((frame (window-frame window)))
              ;; If we chose another frame, make sure it gets input focus.
              (unless (eq frame old-frame)
                (select-frame-set-input-focus frame norecord))
              ;; Make sure the window is selected (Bug#8615), (Bug#6954)
              (select-window window norecord))
          ;; If `display-buffer' failed to supply a window, just make the
          ;; buffer current.
          (set-buffer buffer))
        ;; Return BUFFER even when we got no window.
        buffer))))

(defun switch-to-buffer-other-window-old (buffer-or-name &optional norecord)
  (interactive
   (list (read-buffer-to-switch "Switch to buffer in other window: ")))
  (let ((pop-up-windows t))
    (pop-to-buffer buffer-or-name t norecord)))

(defun org-switch-to-buffer-other-window-old (&rest args)
  (org-no-popups (apply #'switch-to-buffer-other-window-old args)))

(defun org-capture-fill-template (&optional template initial annotation)
  "Fill a TEMPLATE and return the filled template as a string.
The template may still contain \"%?\" for cursor positioning.
INITIAL content and/or ANNOTATION may be specified, but will be overridden
by their respective `org-store-link-plist' properties if present."
  (let* ((template (or template (org-capture-get :template)))
	 (buffer (org-capture-get :buffer))
	 (file (buffer-file-name (or (buffer-base-buffer buffer) buffer)))
	 (time (let* ((c (or (org-capture-get :default-time) (current-time)))
		      (d (decode-time c)))
		 (if (< (nth 2 d) org-extend-today-until)
		     (encode-time 0 59 23 (1- (nth 3 d)) (nth 4 d) (nth 5 d))
		   c)))
	 (v-t (format-time-string (org-time-stamp-format nil) time))
	 (v-T (format-time-string (org-time-stamp-format t) time))
	 (v-u (format-time-string (org-time-stamp-format nil t) time))
	 (v-U (format-time-string (org-time-stamp-format t t) time))
	 (v-c (and kill-ring (current-kill 0)))
	 (v-x (or (org-get-x-clipboard 'PRIMARY)
		  (org-get-x-clipboard 'CLIPBOARD)
		  (org-get-x-clipboard 'SECONDARY)
		  ""))			;ensure it is a string
	 ;; `initial' and `annotation' might have been passed.  But if
	 ;; the property list has them, we prefer those values.
	 (v-i (or (plist-get org-store-link-plist :initial)
		  (and (stringp initial) (org-no-properties initial))
		  (org-capture-get :initial)
		  ""))
	 (v-a
	  (let ((a (or (plist-get org-store-link-plist :annotation)
		       annotation
		       (org-capture-get :annotation)
		       "")))
	    ;; Is the link empty?  Then we do not want it...
	    (if (equal a "[[]]") "" a)))
	 (l-re "\\[\\[\\(.*?\\)\\]\\(\\[.*?\\]\\)?\\]")
	 (v-A (if (and v-a (string-match l-re v-a))
		  (replace-match "[[\\1][%^{Link description}]]" nil nil v-a)
		v-a))
	 (v-l (if (and v-a (string-match l-re v-a))
		  (replace-match "[[\\1]]" nil nil v-a)
		v-a))
	 (v-L (if (and v-a (string-match l-re v-a))
		  (replace-match "\\1" nil nil v-a)
		v-a))
	 (v-n user-full-name)
	 (v-k (if (marker-buffer org-clock-marker)
		  (org-no-properties org-clock-heading)
		""))
	 (v-K (if (marker-buffer org-clock-marker)
		  (org-link-make-string
		   (format "%s::*%s"
			   (buffer-file-name (marker-buffer org-clock-marker))
			   v-k)
		   v-k)
		""))
	 (v-f (or (org-capture-get :original-file-nondirectory) ""))
	 (v-F (or (org-capture-get :original-file) ""))
	 (org-capture--clipboards
	  (delq nil
		(list v-i
		      (org-get-x-clipboard 'PRIMARY)
		      (org-get-x-clipboard 'CLIPBOARD)
		      (org-get-x-clipboard 'SECONDARY)
		      v-c))))
    (setq org-store-link-plist (plist-put org-store-link-plist :annotation v-a))
    (setq org-store-link-plist (plist-put org-store-link-plist :initial v-i))
    (unless template
      (setq template "")
      (message "no template") (ding)
      (sit-for 1))
    (save-window-excursion
      (org-switch-to-buffer-other-window-old (get-buffer-create "*Capture*"))
      (erase-buffer)
      (setq buffer-file-name nil)
      (setq mark-active nil)
      (insert template)
      (goto-char (point-min))
      ;; %[] insert contents of a file.
      (save-excursion
	(while (re-search-forward "%\\[\\(.+\\)\\]" nil t)
	  (let ((filename (expand-file-name (match-string 1)))
		(beg (copy-marker (match-beginning 0)))
		(end (copy-marker (match-end 0))))
	    (unless (org-capture-escaped-%)
	      (delete-region beg end)
	      (set-marker beg nil)
	      (set-marker end nil)
	      (condition-case error
		  (insert-file-contents filename)
		(error
		 (insert (format "%%![couldn not insert %s: %s]"
				 filename
				 error))))))))
      ;; Mark %() embedded elisp for later evaluation.
      (org-capture-expand-embedded-elisp 'mark)
      ;; Expand non-interactive templates.
      (let ((regexp "%\\(:[-A-Za-z]+\\|<\\([^>\n]+\\)>\\|[aAcfFikKlLntTuUx]\\)"))
	(save-excursion
	  (while (re-search-forward regexp nil t)
	    ;; `org-capture-escaped-%' may modify buffer and cripple
	    ;; match-data.  Use markers instead.  Ditto for other
	    ;; templates.
	    (let ((pos (copy-marker (match-beginning 0)))
		  (end (copy-marker (match-end 0)))
		  (value (match-string 1))
		  (time-string (match-string 2)))
	      (unless (org-capture-escaped-%)
		(delete-region pos end)
		(set-marker pos nil)
		(set-marker end nil)
		(let* ((inside-sexp? (org-capture-inside-embedded-elisp-p))
		       (replacement
			(pcase (string-to-char value)
			  (?< (format-time-string time-string time))
			  (?:
			   (or (plist-get org-store-link-plist (intern value))
			       ""))
			  (?i
			   (if inside-sexp? v-i
			     ;; Outside embedded Lisp, repeat leading
			     ;; characters before initial place holder
			     ;; every line.
			     (let ((lead (concat "\n"
						 (org-current-line-string t))))
			       (replace-regexp-in-string "\n" lead v-i nil t))))
			  (?a v-a)
			  (?A v-A)
			  (?c v-c)
			  (?f v-f)
			  (?F v-F)
			  (?k v-k)
			  (?K v-K)
			  (?l v-l)
			  (?L v-L)
			  (?n v-n)
			  (?t v-t)
			  (?T v-T)
			  (?u v-u)
			  (?U v-U)
			  (?x v-x))))
		  (insert
		   (if inside-sexp?
		       ;; Escape sensitive characters.
		       (replace-regexp-in-string "[\\\"]" "\\\\\\&" replacement)
		     replacement))))))))
      ;; Expand %() embedded Elisp.  Limit to Sexp originally marked.
      (org-capture-expand-embedded-elisp)
      ;; Expand interactive templates.  This is the last step so that
      ;; template is mostly expanded when prompting happens.  Turn on
      ;; Org mode and set local variables.  This is to support
      ;; completion in interactive prompts.
      (let ((org-inhibit-startup t)) (org-mode))
      (org-clone-local-variables buffer "\\`org-")
      (let (strings)			; Stores interactive answers.
	(save-excursion
	  (let ((regexp "%\\^\\(?:{\\([^}]*\\)}\\)?\\([CgGLptTuU]\\)?"))
	    (while (re-search-forward regexp nil t)
	      (let* ((items (and (match-end 1)
				 (save-match-data
				   (split-string (match-string-no-properties 1)
						 "|"))))
		     (key (match-string 2))
		     (beg (copy-marker (match-beginning 0)))
		     (end (copy-marker (match-end 0)))
		     (prompt (nth 0 items))
		     (default (nth 1 items))
		     (completions (nthcdr 2 items)))
		(unless (org-capture-escaped-%)
		  (delete-region beg end)
		  (set-marker beg nil)
		  (set-marker end nil)
		  (pcase key
		    ((or "G" "g")
		     (let* ((org-last-tags-completion-table
			     (org-global-tags-completion-table
			      (cond ((equal key "G") (org-agenda-files))
				    (file (list file))
				    (t nil))))
			    (org-add-colon-after-tag-completion t)
			    (ins (mapconcat
				  #'identity
				  (let ((crm-separator "[ \t]*:[ \t]*"))
                                    (completing-read-multiple
				     (if prompt (concat prompt ": ") "Tags: ")
				     org-last-tags-completion-table nil nil nil
				     'org-tags-history))
				  ":")))
		       (when (org-string-nw-p ins)
			 (unless (eq (char-before) ?:) (insert ":"))
			 (insert ins)
			 (unless (eq (char-after) ?:) (insert ":"))
			 (when (org-at-heading-p) (org-align-tags)))))
		    ((or "C" "L")
		     (let ((insert-fun (if (equal key "C") #'insert
					 (lambda (s) (org-insert-link 0 s)))))
		       (pcase org-capture--clipboards
			 (`nil nil)
			 (`(,value) (funcall insert-fun value))
			 (`(,first-value . ,_)
			  (funcall insert-fun
				   (read-string "Clipboard/kill value: "
						first-value
						'org-capture--clipboards
						first-value)))
			 (_ (error "Invalid `org-capture--clipboards' value: %S"
				   org-capture--clipboards)))))
		    ("p"
		     ;; We remove keyword properties inherited from
		     ;; target buffer so `org-read-property-value' has
		     ;; a chance to find allowed values in sub-trees
		     ;; from the target buffer.
		     (setq-local org-keyword-properties nil)
		     (let* ((origin (set-marker (make-marker)
						(org-capture-get :pos)
						(org-capture-get :buffer)))
			    ;; Find location from where to get allowed
			    ;; values.  If `:target-entry-p' is
			    ;; non-nil, the current headline in the
			    ;; target buffer is going to be a parent
			    ;; headline, so location is fine.
			    ;; Otherwise, find the parent headline in
			    ;; the target buffer.
			    (pom (if (org-capture-get :target-entry-p) origin
				   (let ((level (progn
						  (while (org-up-heading-safe))
						  (org-current-level))))
				     (org-with-point-at origin
				       (let ((l (if (org-at-heading-p)
						    (org-current-level)
						  most-positive-fixnum)))
					 (while (and l (>= l level))
					   (setq l (org-up-heading-safe)))
					 (if l (point-marker)
					   (point-min-marker)))))))
			    (value
			     (org-read-property-value prompt pom default)))
		       (org-set-property prompt value)))
		    ((or "t" "T" "u" "U")
		     ;; These are the date/time related ones.
		     (let* ((upcase? (equal (upcase key) key))
			    (org-end-time-was-given nil)
			    (time (org-read-date upcase? t nil prompt)))
		       (org-insert-time-stamp
			time (or org-time-was-given upcase?)
			(member key '("u" "U"))
			nil nil (list org-end-time-was-given))))
		    (`nil
		     ;; Load history list for current prompt.
		     (setq org-capture--prompt-history
			   (gethash prompt org-capture--prompt-history-table))
		     (push (org-completing-read
			    (concat (or prompt "Enter string")
				    (and default (format " [%s]" default))
				    ": ")
			    completions
			    nil nil nil 'org-capture--prompt-history default)
			   strings)
		     (insert (car strings))
		     ;; Save updated history list for current prompt.
		     (puthash prompt org-capture--prompt-history
			      org-capture--prompt-history-table))
		    (_
		     (error "Unknown template placeholder: \"%%^%s\""
			    key))))))))
	;; Replace %n escapes with nth %^{...} string.
	(setq strings (nreverse strings))
	(save-excursion
	  (while (re-search-forward "%\\\\\\([1-9][0-9]*\\)" nil t)
	    (unless (org-capture-escaped-%)
	      (replace-match
	       (nth (1- (string-to-number (match-string 1))) strings)
	       nil t)))))
      ;; Make sure there are no empty lines before the text, and that
      ;; it ends with a newline character or it is empty.
      (skip-chars-forward " \t\n")
      (delete-region (point-min) (line-beginning-position))
      (goto-char (point-max))
      (skip-chars-backward " \t\n")
      (if (bobp) (delete-region (point) (line-end-position))
	(end-of-line)
	(delete-region (point) (point-max))
	(insert "\n"))
      ;; Return the expanded template and kill the capture buffer.
      (untabify (point-min) (point-max))
      (set-buffer-modified-p nil)
      (prog1 (buffer-substring-no-properties (point-min) (point-max))
	(kill-buffer (current-buffer))))))

;; (defun pop-to-buffer (buffer-or-name &optional action norecord)
;;   "Wow"
;;   (interactive (list (read-buffer "Pop to buffer: " (other-buffer))
;; 		     (if current-prefix-arg t)))
;;   (let* ((buffer (window-normalize-buffer-to-switch-to buffer-or-name))
;;          (old-frame (selected-frame))
;; 	 (window (xgo-return-window buffer action)))
;;     ;; Don't assume that `display-buffer' has supplied us with a window
;;     ;; (Bug#24332).
;;     (if window
;;         (let ((frame (window-frame window)))
;;           ;; If we chose another frame, make sure it gets input focus.
;;           (unless (eq frame old-frame)
;;             (select-frame-set-input-focus frame norecord))
;;           ;; Make sure the window is selected (Bug#8615), (Bug#6954)
;;           (select-window window norecord))
;;       ;; If `display-buffer' failed to supply a window, just make the
;;       ;; buffer current.
;;       (set-buffer buffer))
;;     ;; Return BUFFER even when we got no window.
;;     buffer))

;; (defun i3-display-buffer-use-some-frame (buffer alist)
;;   (ignore alist)
;;   (when (and (display-graphic-p)
;;              (not (or (member (buffer-name buffer) '("*Completions*" " *undo-tree*"))
;;                       (string-match-p "\\`[*][Hh]elm.*[*]\\'" (buffer-name buffer)))))
;;     (xgo-return-window buffer)
;;     ;; (let* ((frame (i3-get-popup-frame-for-buffer buffer))
;;     ;;        (window (i3-get-window-for-frame frame)))
;;     ;;   (window--display-buffer buffer window 'reuse))
;;     ))

;;--------------------------------------------------------------------------------
;; The ultimate of xgo technology
;;--------------------------------------------------------------------------------

;; TODO this is kinda sketchy
(setq display-buffer-overriding-action '(nil . nil))
(cl-pushnew 'i3-xgo-display-buffer (car display-buffer-overriding-action))
;; (setq display-buffer-alist '((".*" xgo-return-window)))

(defun display-buffer (buffer-or-name &optional action frame)
  (interactive (list (read-buffer "Display buffer: " (other-buffer))
		     (if current-prefix-arg t)))
  (let ((buffer (if (bufferp buffer-or-name)
		    buffer-or-name
		  (get-buffer buffer-or-name)))
	;; Make sure that when we split windows the old window keeps
	;; point, bug#14829.
	(split-window-keep-point t)
	;; Handle the old form of the first argument.
	(inhibit-same-window (and action (not (listp action)))))
    (unless (listp action) (setq action nil))
    (if display-buffer-function
	;; If `display-buffer-function' is defined, let it do the job.
	(funcall display-buffer-function buffer inhibit-same-window)
      ;; Otherwise, use the defined actions.
      (let* ((user-action
	      (xgo-display-buffer-assq-regexp
	       (buffer-name buffer) xgo-display-buffer-alist))
             (special-action (display-buffer--special-action buffer))
	     ;; Extra actions from the arguments to this function:
	     (extra-action
	      (cons nil (append (if inhibit-same-window
				    '((inhibit-same-window . t)))
				(if frame
				    `((reusable-frames . ,frame))))))
	     ;; Construct action function list and action alist.
	     (actions (list display-buffer-overriding-action
			    user-action special-action action extra-action
			    display-buffer-base-action
			    display-buffer-fallback-action))
	     (functions (apply 'append
			       (mapcar (lambda (x)
					 (setq x (car x))
					 (if (functionp x) (list x) x))
				       actions)))
	     (alist (apply 'append (mapcar 'cdr actions)))
	     window)
	(unless (buffer-live-p buffer)
	  (error "Invalid buffer"))
	(while (and functions (not window))
	  (setq window (funcall (car functions) buffer alist)
	  	functions (cdr functions)))
	(and (windowp window) window)))))

(setq xgo-display-buffer-alist '(("\\`[*][Hh]elm.*[*]\\'" i3-display-buffer-use-some-frame)
				 ("Org todo" i3-display-buffer-use-some-frame)
				 ("magit:.*" xgo-return-window)
				 (".*eldoc.*" i3-display-buffer-use-some-frame)
				 (".*transient.*" i3-display-buffer-use-some-frame)
				 (".*magit-diff.*" i3-display-buffer-use-some-frame)
				 ("CAPTURE.*org" i3-display-buffer-use-some-frame) ;TODO rsw fix this
				 (".*Backtrace.*" i3-display-buffer-use-some-frame)
				 (".*" xgo-return-window)))

(defun xgo-display-buffer-assq-regexp (buffer-name alist)
  (catch 'match
    (dolist (entry alist)
      (let ((key (car entry)))
	(when (and (stringp key)
		   (string-match-p key buffer-name))
	  (throw 'match (cdr entry)))))))

(defun i3-xgo-display-buffer (buffer alist)
  (ignore alist)
  (when (and (display-graphic-p)
             (not (or (member (buffer-name buffer) '("*Completions*" " *undo-tree*"))
                      (string-match-p "\\`[*][Hh]elm.*[*]\\'" (buffer-name buffer)))))
    (let ((user-func (xgo-display-buffer-assq-regexp
		      (buffer-name buffer) xgo-display-buffer-alist)))
      (if user-func (funcall (car user-func) buffer alist)
	(xgo-return-window buffer alist)))))


(defun rsw-write-to-scratch-buffer (text)
  "Write TEXT to a new line in the *scratch* buffer."
  (with-current-buffer "*scratch*"
    (goto-char (point-max))    ;; Move to the end of the buffer
    (unless (bolp)
      (newline))               ;; Insert a newline if not already at beginning of line
    (insert (prin1-to-string text))              ;; Insert the text
    (newline)))

(defun xref--show-pos-in-buf (pos buf)
  "Goto and display position POS of buffer BUF in a window.
Honor `xref--original-window-intent', run `xref-after-jump-hook'
and finally return the window."
  (let* ((pop-up-frames
          (or (eq xref--original-window-intent 'frame)
              pop-up-frames))
         (action
          (cond ((eq xref--original-window-intent 'frame)
                 t)
                ((eq xref--original-window-intent 'window)
                 `((xref--display-buffer-in-other-window)
                   (window . ,xref--original-window)))
                ((and
                  (window-live-p xref--original-window)
                  (or (not (window-dedicated-p xref--original-window))
                      (eq (window-buffer xref--original-window) buf)))
                 `((xref--display-buffer-in-window)
                   (window . ,xref--original-window))))))
    (rsw-xgo-buffer buf)
    (xref--goto-char pos)
    (run-hooks 'xref-after-jump-hook)))

(defun xref-pop-to-location (item &optional action)
  "Go to the location of ITEM and display the buffer.
ACTION controls how the buffer is displayed:
  nil      -- `switch-to-buffer'
  `window' -- `pop-to-buffer' (other window)
  `frame'  -- `pop-to-buffer' (other frame)
If SELECT is non-nil, select the target window."
  (let* ((marker (save-excursion
                   (xref-location-marker (xref-item-location item))))
         (buf (marker-buffer marker)))
    (cl-ecase action
      ((nil)  (switch-to-buffer buf))
      (window (rsw-xgo-buffer buf))
      (frame  (let ((pop-up-frames t)) (pop-to-buffer buf t))))
    (xref--goto-char marker))
  (let ((xref--current-item item))
    (run-hooks 'xref-after-jump-hook)))

(defun flycheck-jump-in-buffer (buffer error)
  "In BUFFER, jump to ERROR."
  ;; FIXME: we assume BUFFER and the buffer of ERROR are the same.  We don't
  ;; need the first argument then.
  (if (eq (window-buffer) (get-buffer flycheck-error-list-buffer))
      ;; When called from within the error list, keep the error list,
      ;; otherwise replace the current buffer.
      (rsw-xgo-buffer buffer)
    (switch-to-buffer buffer))
  (let ((pos (flycheck-error-pos error)))
    (unless (eq (goto-char pos) (point))
      ;; If widening gets in the way of moving to the right place, remove it
      ;; and try again
      (widen)
      (goto-char pos)))
  ;; Re-highlight the errors.  We have post-command-hook for that, but calls to
  ;; `flycheck-jump-in-buffer' that come from other buffers (e.g. from the error
  ;; list) won't trigger it.
  (flycheck-error-list-highlight-errors 'preserve-pos))

(lsp-treemacs-define-action lsp-treemacs-symbols-goto-symbol (:location)
  "Goto the symbol node at `point'."
  (rsw-xgo-buffer lsp-treemacs--symbols-last-buffer)
  (goto-char (lsp--position-to-point location))
  (run-hooks 'lsp-treemacs-after-jump-hook))

(defun arrayify (start end quote)
  "Turn strings on newlines into a QUOTEd, comma-separated one-liner.
Ripped shamelessly from hacker news https://news.ycombinator.com/item?id=22131815"
  (interactive "r\nMQuote: ")
  (let ((insertion
         (mapconcat
          (lambda (x) (format "%s%s%s" quote x quote))
          (split-string (buffer-substring start end)) ", ")))
    (delete-region start end)
    (insert insertion)))


(cl-defun lsp-find-references (&optional exclude-declaration &key display-action)
  "Find references of the symbol under point."
  (interactive "P")
  (lsp-find-locations "textDocument/references"
                      (list :context `(:includeDeclaration
				       ,(lsp-json-bool
					 (not
					  (or
					   (not exclude-declaration)
					   lsp-references-exclude-definition)))))
                      :display-action display-action
                      :references? t))

(defun xgo--show-xref-buffer (fetcher alist)
  (cl-assert (functionp fetcher))
  (let* ((xrefs
          (or
           (assoc-default 'fetched-xrefs alist)
           (funcall fetcher)))
         (xref-alist (xref--analyze xrefs))
         (dd default-directory)
         buf)

    (if
	(not (cdr xrefs))
	(xref-pop-to-location (car xrefs)
                              (assoc-default 'display-action alist))
      (progn
	(with-current-buffer (get-buffer-create xref-buffer-name)
	  (xref--ensure-default-directory dd (current-buffer))
	  (xref--xref-buffer-mode)
	  (xref--show-common-initialize xref-alist fetcher alist)
	  (pop-to-buffer (current-buffer))
	  (setq buf (current-buffer)))
	(xref--auto-jump-first buf (assoc-default 'auto-jump alist))))
    buf))

(defun rsw-xgo-find-file (filename &optional wildcards)
  "Edit file FILENAME, in another window.

Like \\[find-file] (which see), but creates a new window or reuses
an existing one.  See the function `display-buffer'.

Interactively, the default if you just type RET is the current directory,
but the visited file name is available through the minibuffer history:
type \\[next-history-element] to pull it into the minibuffer.

The first time \\[next-history-element] is used after Emacs prompts for
the file name, the result is affected by `file-name-at-point-functions',
which by default try to guess the file name by looking at point in the
current buffer.  Customize the value of `file-name-at-point-functions'
or set it to nil, if you want only the visited file name and the
current directory to be available on first \\[next-history-element]
request.

Interactively, or if WILDCARDS is non-nil in a call from Lisp,
expand wildcards (if any) and visit multiple files."
  (interactive
   (find-file-read-args "Find file in other window: "
                        (confirm-nonexistent-file-or-buffer)))
  (let ((value (find-file-noselect filename nil nil wildcards)))
    (if (listp value)
	(progn
	  (setq value (nreverse value))
	  (rsw-xgo-buffer (car value))
	  (mapc 'switch-to-buffer (cdr value))
	  value)
      (rsw-xgo-buffer value))))

(defun dired--find-possibly-alternative-file (file)
  "Find FILE, but respect `dired-kill-when-opening-new-dired-buffer'."
  (if (and dired-kill-when-opening-new-dired-buffer
           (file-directory-p file))
      (progn
        (set-buffer-modified-p nil)
        (dired--find-file #'find-alternate-file file))
    (dired--find-file #'rsw-xgo-find-file file)))

(defun org-roam-id-open (id _)
  "Go to the entry with id ID.
Like `org-id-open', but additionally uses the Org-roam database."
  (org-mark-ring-push)
  (let ((m (or (org-roam-id-find id 'marker)
               (org-id-find id 'marker)))
        cmd)
    (unless m
      (error "Cannot find entry with ID \"%s\"" id))
    ;; Use a buffer-switching command in analogy to finding files
    (setq cmd 'rsw-xgo-buffer)
    (if (not (equal (current-buffer) (marker-buffer m)))
        (funcall cmd (marker-buffer m)))
    (goto-char m)
    (move-marker m nil)
    (org-show-context)))

(defun magit-display-buffer (buffer &optional display-function)
  (rsw-xgo-buffer buffer))

;;--------------------------------------------------------------------------------
;; makes headerline frame local
;;--------------------------------------------------------------------------------

;; (require 'lsp-headerline)

;; (define-minor-mode lsp-headerline-breadcrumb-mode
;;   "Toggle breadcrumb on headerline."
;;   :group 'lsp-headerline
;;   :global nil
;;   (cond
;;    (lsp-headerline-breadcrumb-mode
;;     ;; make sure header-line-format, if non-nil, is a list.  as
;;     ;; mode-line-format says: "The value may be nil, a string, a
;;     ;; symbol or a list."
;;     (unless (listp header-line-format)
;;       (setq header-line-format (list header-line-format)))
;;     (add-to-list 'header-line-format '(t (:eval (frame-parameter nil 'lsp-headerline--string) )))

;;     (add-hook 'xref-after-jump-hook #'lsp-headerline--check-breadcrumb nil t)

;;     (add-hook 'lsp-on-idle-hook #'lsp-headerline--check-breadcrumb nil t)
;;     (add-hook 'lsp-configure-hook #'lsp-headerline--enable-breadcrumb nil t)
;;     (add-hook 'lsp-unconfigure-hook #'lsp-headerline--disable-breadcrumb nil t))
;;    (t
;;     (remove-hook 'lsp-on-idle-hook #'lsp-headerline--check-breadcrumb t)
;;     (remove-hook 'lsp-configure-hook #'lsp-headerline--enable-breadcrumb t)
;;     (remove-hook 'lsp-unconfigure-hook #'lsp-headerline--disable-breadcrumb t)

;;     (remove-hook 'xref-after-jump-hook #'lsp-headerline--check-breadcrumb t)

;;     (setq lsp-headerline--path-up-to-project-segments nil)
;;     (setq header-line-format (remove '(t (:eval (frame-parameter nil 'lsp-headerline--string) )) header-line-format)))))

;; (defun lsp-headerline--check-breadcrumb (&rest _)
;;   "Request for document symbols to build the breadcrumb."
;;   (set-frame-parameter (selected-frame) 'lsp-headerline--string (lsp-headerline--build-string))
;;   (force-mode-line-update))


;;--------------------------------------------------------------------------------
;; I find moving to be my usual use case, especially with the calling function bound to H-m
;;--------------------------------------------------------------------------------

(defvar aw-dispatch-alist
  '((?x aw-delete-window "Delete Window")
    (?M aw-swap-window "Swap Windows")
    (?m aw-move-window "Move Window")
    (?c aw-copy-window "Copy Window")
    (?j aw-switch-buffer-in-window "Select Buffer")
    (?n aw-flip-window)
    (?u aw-switch-buffer-other-window "Switch Buffer Other Window")
    (?e aw-execute-command-other-window "Execute Command Other Window")
    (?F aw-split-window-fair "Split Fair Window")
    (?v aw-split-window-vert "Split Vert Window")
    (?b aw-split-window-horz "Split Horz Window")
    (?o delete-other-windows "Delete Other Windows")
    (?T aw-transpose-frame "Transpose Frame")
    ;; ?i ?r ?t are used by hyperbole.el
    (?? aw-show-dispatch-help))
  "List of actions for `aw-dispatch-default'.
Each action is a list of either:
  (char function description) where function takes a single window argument
or
  (char function) where function takes no argument and the description is omitted.")


;;--------------------------------------------------------------------------------
;; because pasting reformats neighbors sometimes
;;--------------------------------------------------------------------------------

(defun ravar-insert ()
  (interactive)
  (insert (substring-no-properties (car kill-ring))))

;;--------------------------------------------------------------------------------
;; TODO don't publish this lol
;;--------------------------------------------------------------------------------

(defun eldoc--format-doc-buffer (docs)
  "Ensure DOCS are displayed in an *eldoc* buffer."
  (with-current-buffer (if (buffer-live-p eldoc--doc-buffer)
                           eldoc--doc-buffer
                         (setq eldoc--doc-buffer
                               (get-buffer-create " *eldoc*")))
    (unless (eq docs eldoc--doc-buffer-docs)
      (setq-local eldoc--doc-buffer-docs docs)
      (let ((inhibit-read-only t)
            (things-reported-on))
        (erase-buffer) (setq buffer-read-only t)
        (setq-local nobreak-char-display nil)
        (local-set-key "q" 'quit-window)
        (cl-loop for (docs . rest) on docs
                 for (this-doc . plist) = docs
                 for thing = (plist-get plist :thing)
                 when thing do
                 (cl-pushnew thing things-reported-on)
                 (setq this-doc
                       (concat
                        (propertize (format "%s" thing)
                                    'face (plist-get plist :face))
                        ": "
                        this-doc))
                 do (insert this-doc)
                 when rest do (insert "\n")
                 finally (goto-char (point-min)))
        ;; Rename the buffer, taking into account whether it was
        ;; hidden or not
	;; (write-region (concat "|<eldoc-buffer-contents>|\n"
	;; 		      (buffer-string)
	;; 		      "\n|<end>|\n")
	;; 	      nil wow-file 'append 'visit)
        (rename-buffer (format "%s*eldoc%s*"
                               (if (string-match "^ " (buffer-name)) " " "")
                               (if things-reported-on
                                   (format " for %s"
                                           (mapconcat
                                            (lambda (s) (format "%s" s))
                                            things-reported-on
                                            ", "))
                                 ""))))))
  eldoc--doc-buffer)


(defun diff-buffers-without-temp-files (buffer1 buffer2 &optional switches)
  "Run diff program on BUFFER1 and BUFFER2.
    Make the comparison without the creation of temporary files.

    When called interactively with a prefix argument, prompt
    interactively for diff switches.  Otherwise, the switches
    specified in the variable `diff-switches' are passed to the diff command."
  (interactive
   (list (read-buffer "buffer1: " (current-buffer))
         (read-buffer "buffer2: " (current-buffer))
         (diff-switches)))
  (or switches (setq switches diff-switches))
  (unless (listp switches) (setq switches (list switches)))
  (let ((buffers (list buffer1 buffer2))
        (buf (get-buffer-create "*diff-buffers*"))
        fifos res)
    (dotimes (_ 2) (push (make-temp-name "/tmp/pipe") fifos))
    (setq fifos (nreverse fifos))
    (with-current-buffer buf (erase-buffer))
    (unwind-protect
        (progn
          (dotimes (i 2)
            (let ((cmd (format "cat > %s << EOF\n%s\nEOF"
                               (nth i fifos)
                               (with-current-buffer (nth i buffers)
                                 (buffer-string)))))
              (call-process "mkfifo" nil nil nil (nth i fifos))
              (start-process-shell-command (format "p%d" i) nil cmd)))
          (setq res (apply #'call-process diff-command nil buf nil (car fifos) (cadr fifos) switches))
          (if (zerop res)
              (message "Buffers have same content")
            (display-buffer buf)
            (with-current-buffer buf (diff-mode))
	    (message "Buffer contents are different"))
          res)
      ;; Clean up.
      (dolist (x fifos)
        (and (file-exists-p x) (delete-file x))))))

;;--------------------------------------------------------------------------------
;; from the emacs wiki itself https://www.emacswiki.org/emacs/IncrementNumber
;;--------------------------------------------------------------------------------

(defun my-increment-number-at-point (&optional increment)
  "Increment the number at point by INCREMENT."
  (interactive "*p")
  (let ((pos (point)))
    (save-match-data
      (skip-chars-backward "0-9")
      (if (looking-at "[0-9]+")
          (let ((field-width (- (match-end 0) (match-beginning 0)))
                (newval (+ (string-to-number (match-string 0) 10) increment)))
            (when (< newval 0)
              (setq newval (+ (expt 10 field-width) newval)))
            (replace-match (format (concat "%0" (int-to-string field-width) "d")
                                   newval)))
        (user-error "No number at point")))
    (goto-char pos)))

(defun my-decrement-number-at-point (&optional decrement)
  "Decrement the number at point by DECREMENT."
  (interactive "*p")
  (my-increment-number-at-point (- decrement)))

(defhydra hydra-inc (global-map "H-;")
  "Increment/decrement"
  ("j" my-decrement-number-at-point "decrement number")
  ("k" my-increment-number-at-point "increment number"))

(provide 'ravar-custom) 


