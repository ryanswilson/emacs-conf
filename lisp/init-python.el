
(maybe-require-package 'pyim)
(maybe-require-package 'pyim-basedict)
(pyim-basedict-enable)

(maybe-require-package 'pyvenv)
(maybe-require-package 'blacken)

(require 'python)
(add-hook 'python-mode-hook 'blacken-mode)
(define-key python-mode-map (kbd "-") (lambda () (interactive) (insert-char #x5f)))
(define-key python-mode-map (kbd "_") (lambda () (interactive) (insert-char #x2d)))
;; (define-key rust-mode-map (kbd "-") (lambda () (interactive) (insert-char #x5f)))
;; (define-key rust-mode-map (kbd "_") (lambda () (interactive) (insert-char #x2d)))
(define-key python-mode-map (kbd "M-.") 'lsp-find-references)

(defun my/disable-ac-mode ()
  (interactive)
  (auto-complete-mode -1))

(add-hook 'python-mode-hook 'my/disable-ac-mode)
(add-hook 'lsp-mode-hook 'my/disable-ac-mode)

;; (flycheck-add-next-checker 'python-mypy '(t . lsp))

(maybe-require-package 'python-isort)
(add-hook 'python-mode-hook 'python-isort-on-save-mode)

;; (flycheck-add-next-checker 'python-mypy '(t . lsp))
;; (flycheck-add-next-checker 'python-flake8 '(t . python-mypy))
;; (setq flycheck-checker 'python-flake8)
;; (setq flycheck-checker 'python-mypy)
;; disable pylint checker enable flake8 check everything enabled correctly, figure out how to setup

(provide 'init-python)
