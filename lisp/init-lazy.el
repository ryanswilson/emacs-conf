(defun rsw-lazy-writer ()
  (interactive)
  (let* ((og-buf (current-buffer))
	 (url-request-method
	  (encode-coding-string "POST" 'us-ascii))
	 (url-request-data
	  (buffer-substring-no-properties 1 (point)))
	 (url-request-extra-headers
	  '(("Content-Type" . "text/plain"))))
    (url-retrieve
     "http://localhost:8000/generate"
     'my-switch-to-url-buffer
     (list og-buf))))

(defun my-switch-to-url-buffer (status og-buf)
  (let ((new-buf (current-buffer)))
    (with-current-buffer og-buf
      (insert
       (lazy-writer-format-text
	(with-current-buffer new-buf
	  (goto-char 0)
	  (re-search-forward "|<start>|")
	  (buffer-substring-no-properties
	   (point) (1- (point-max)))))))))

(defun lazy-writer-format-text (text)
  (let* ((text (replace-regexp-in-string "\\\\n" "\n" text))
	 (text-2
	  (replace-regexp-in-string "\\\\\"" "\"" text)))
	 (replace-regexp-in-string "—" "-" text-2)))

(defvar lazy-writer-mode-map
  (let ((map (make-sparse-keymap)))
    ;; Define keybindings here
    (define-key map (kbd "M-l") 'rsw-lazy-writer)
    map)
  "Keymap for Lazy Writer")

(define-minor-mode lazy-writer-mode
  "Lazy Writer mode"
  :init-value nil
  :lighter " lw"
  :keymap lazy-writer-mode-map)
