;; "Inspired heavily" by https://github.com/purcell/emacs.d/blob/master/init.el

;;--------------------------------------------------------------------------------
;; From here to close all comes from purcell

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

;; (setq normal-gc-cons-threshold (* 20 1024 1024))
;; (let ((init-gc-cons-threshold (* 1024 1024 1024)))
;;   (setq gc-cons-threshold init-gc-cons-threshold)
;;   (add-hook 'emacs-startup-hook
;;            (lambda () (setq gc-cons-threshold normal-gc-cons-threshold))))

;; (gcmh-mode 1)
;; (gcmh-mode 0)
(setq gcmh-high-cons-threshold (* 1024 1024 1024 20))
;; (garbage-collect)
;; TODO rsw what was this

(require 'init-utils)
(require 'init-site-lisp) ;; Must come before elpa, as it may provide package.el
;; Calls (package-initialize)
(require 'init-elpa)      ;; Machinery for installing required packages
(require 'init-exec-path) ;; Set up $PATH

;;--------------------------------------------------------------------------------

(maybe-require-package 'use-package)
(require 'use-package)

;;--------------------------------------------------------------------------------
;; get some packages by git
;;--------------------------------------------------------------------------------
(require 'init-git)


;;--------------------------------------------------------------------------------
;; package "agnostic" configs
;;--------------------------------------------------------------------------------

;;who thought this was a good idea
(setq ring-bell-function 'ignore)

(global-set-key (kbd "C-c C-c C-k") 'kill-emacs)

(global-set-key (kbd "H-b") 'switch-to-prev-buffer)
(global-set-key (kbd "H-f") 'switch-to-next-buffer)
(global-set-key (kbd "H-n") 'switch-to-next-buffer)
(global-set-key (kbd "H-p") 'switch-to-prev-buffer)

(maybe-require-package 'pomm)
(global-set-key (kbd "H-t") 'pomm-third-time)

(electric-pair-mode 1)

(global-display-line-numbers-mode)

;; (auto-revert-mode)

(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

(fset 'yes-or-no-p 'y-or-n-p)
(show-paren-mode 1)

(defun move-line-up ()
  "Move up the current line."
  (interactive)
  (transpose-lines 1)
  (forward-line -2)
  (indent-according-to-mode))

(defun move-line-down ()
  "Move down the current line."
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (indent-according-to-mode))

(global-set-key (kbd "C-S-p") 'move-line-up)
(global-set-key (kbd "C-S-n") 'move-line-down)

;;makes the init file prettier
(font-lock-add-keywords 'emacs-lisp-mode '(("(\\(maybe-require-package\\)\\_>[ 	']*\\(\\(?:\\sw\\|\\s_\\|\\\\.\\)+\\)?"
			       (1 font-lock-keyword-face)
			       (2 font-lock-constant-face ))))

(set-face-attribute 'default nil :height 105)
(global-set-key (kbd "H--") 'text-scale-adjust)
(global-set-key (kbd "H-=") 'text-scale-adjust)

(column-number-mode)
(setq global-mark-ring-max 500)

(setq completions-detailed t)

(setq-default show-trailing-whitespace nil)

;;--------------------------------------------------------------------------------
;; This has the packages that need to be install by user, or not applicable to
;; everyone, like i3 integration
;;--------------------------------------------------------------------------------
(require 'init-optional)

;;--------------------------------------------------------------------------------
;; package soup
;;-------------------------------------------------------------------------------- 

(maybe-require-package 'unicode-fonts)
(maybe-require-package 'with-editor)
(maybe-require-package 'yasnippet)
(maybe-require-package 'monokai-theme)
(load-theme 'monokai t)
;;super bright, but does handle org
;; (load-theme 'leuven t)

(maybe-require-package 'j-mode)
(maybe-require-package 'which-key)
(which-key-mode 1)

(maybe-require-package 'dash)
(git-ensure-package "https://github.com/Fuco1/dired-hacks.git" "dired-hacks")
(require 'dired-subtree)
(define-key dired-mode-map "i" 'dired-subtree-insert)
(define-key dired-mode-map ";" 'dired-subtree-remove)

(maybe-require-package 'key-chord)
(setq key-chord-two-keys-delay 0.2)
(maybe-require-package 'dap-mode)
(maybe-require-package 'company)
(maybe-require-package 'flycheck)
(require 'eldoc)

(progn
  (require 'dap-go)
  (global-set-key [f6] 'dap-breakpoint-toggle)
  (global-set-key [f7] 'dap-next)
  (global-set-key [f8] 'dap-step-in)
  (global-set-key [f9] 'dap-continue)
  (define-key dap-mode-map (kbd "H-e") 'dap-eval)
  (define-key dap-mode-map (kbd "H-h") 'dap-hydra))
;; (dap-ui-controls-mode 0)

(git-ensure-package "https://depp.brause.cc/chuck-mode.git" "chuck-mode")
(require 'chuck-mode)

;;https://github.com/emacsmirror/emacswiki.org/blob/master/isearch-prop.el
;;https://github.com/emacsmirror/emacswiki.org/blob/master/isearch%2b.el
;; (eval-after-load "isearch" '(require 'isearch+))
;; (add-to-list 'load-path (expand-file-name "addedPackages" user-emacs-directory))

;;TODO URGENT fix
(require 'init-rust)

;; (define-key view-mode-map (kbd "C-j") 'avy-goto-word-or-subword-1)

(require 'init-go)

(require 'org-roam-setup)

;; (lsp-register-client
;;  (make-lsp-client :new-connection
;;                   (lsp-stdio-connection (lambda ()
;;                                           (cons lsp-clients-deno-server
;;                                                 lsp-clients-deno-server-args)))
;;                   :initialization-options #'lsp-clients-deno--make-init-options
;;                   :priority 56
;;                   :activation-fn #'lsp-typescript-javascript-tsx-jsx-activate-p
;;                   :server-id 'deno-ls))


;; (defun lsp-clients-deno--make-init-options ()
;;   "Initialization options for the Deno language server."
;;   `(:enable t
;;     :config "/home/ryanwilson/everlaw/servers/assets/src/main/webapp/Javascript/deno.json"
;;     :importMap ,lsp-clients-deno-import-map
;;     :lint ,(lsp-json-bool lsp-clients-deno-enable-lint)
;;     :unstable ,(lsp-json-bool lsp-clients-deno-enable-unstable)
;;     :codeLens (:implementations ,(lsp-json-bool lsp-clients-deno-enable-code-lens-implementations)
;;                :references ,(lsp-json-bool (or lsp-clients-deno-enable-code-lens-references
;;                                                lsp-clients-deno-enable-code-lens-references-all-functions))
;;                :referencesAllFunctions ,(lsp-json-bool lsp-clients-deno-enable-code-lens-references-all-functions))))

;; \(Everlaw/.*\)\"
;; \1.ts"
;; (setq lsp-eslint-server-command '("node" "/usr/lib/node_modules/vscode-langservers-extracted/lib/eslint-language-server/eslintServer.js" "--stdio"))
;; (setq lsp-eslint-server-command '("eslint-lsp" "--stdio"))
;; (setq lsp-eslint-server-command '("vscode-eslint-language-server" "--stdio"))
;; (setq lsp-eslint-trace-server "verbose")
;; (setq lsp-clients-deno-config "/home/ryanwilson/everlaw/servers/assets/src/main/webapp/Javascript/deno.json")
(setq lsp-disabled-clients '(jdtls kotlin-ls))
(setq lsp-enabled-clients '())
;; (setq lsp-enabled-clients '(ts-ls
;;                             pyright
;;                             eslint
;;                                   ))

(with-eval-after-load 'lsp-mode
  (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]\\.env\\'")
  (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]\\target\\'"))

(with-eval-after-load 'lsp-mode
  ;; :global/:workspace/:file
  (setq lsp-modeline-diagnostics-scope :workspace))

(use-package tree-sitter
  :ensure t
  :config
  ;; activate tree-sitter on any buffer containing code for which it has a parser available
  (global-tree-sitter-mode)
  ;; you can easily see the difference tree-sitter-hl-mode makes for python, ts or tsx
  ;; by switching on and off
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))

(use-package tree-sitter-langs
  :ensure t
  :after tree-sitter)

;; (require 'lsp-java)
;; (add-hook  'java-mode-hook #'lsp)
;; (use-package java-mode
;;   :bind (:map java-mode-map
;; 	      ("C-." . lsp-find-definition)
;; 	      ("M-." . lsp-find-references)
;;               ("C-c C-b C-t" . bm-toggle)
;;               ("C-c C-b C-n" . bm-next)
;;               ("C-c C-b C-p" . bm-previous)))
;; (setq indent-tabs-mode nil)
(global-set-key (kbd "C-c C-.") 'xref-find-definitions-other-window)

(require 'column-marker)
(defun run-col-3 ()
  "hopefully adds column marker"
  (interactive)
  (column-marker-3 100))
;; (add-hook 'java-mode-hook 'run-col-3)

(use-package dap-mode :after lsp-mode :config (dap-auto-configure-mode))
;; (use-package dap-java :ensure nil) 	;TODO working??
;; (setq lsp-keymap-prefix (kbd "C-c l")) ;; moved to custom.el
(define-key lsp-mode-map (kbd "C-c l") lsp-command-map)

;; (use-package lsp-mode
;;   :commands lsp
;;   :config (require 'lsp-clients))
;; (use-package lsp-ui)
;; don't like lsp, its too clever by half
;; warning about ptrace scope you unhardend this

(setq backup-directory-alist '(("" . "~/.emacs.d/backup")))

(setq user-temporary-file-directory "~/.emacs.d/tsave/")
(make-directory user-temporary-file-directory t)
(setq auto-save-list-file-prefix
      (concat user-temporary-file-directory ".auto-saves-"))
(setq auto-save-file-name-transforms
      `((".*" ,user-temporary-file-directory t)))
(setq create-lockfiles nil)


;;this is set to make mu4e legible if you ever use it
(setq shr-color-visible-luminance-min 60)
(setq shr-color-visible-distance-min 5)
(setq shr-use-colors nil)
(advice-add #'shr-colorize-region :around (defun shr-no-colourise-region (&rest ignore)))
(when (fboundp 'imagemagick-register-types)
  (imagemagick-register-types))

;; (maybe-require-package 'ob-ipython)
(maybe-require-package 'clojure-mode)
;; (require 'ob-python)

(maybe-require-package 'jupyter)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((clojure . t)
   (jupyter . t)))

(setq auto-window-vscroll nil)

(add-hook 'org-babel-after-execute-hook 'org-display-inline-images 'append)
(setq org-confirm-babel-evaluate nil)
(global-set-key (kbd "H-[") 'org-agenda)
;;typing latex drives me up a wall
(key-chord-define org-mode-map "qi" '(lambda () (interactive) (insert "\\")))
(key-chord-mode 1)

(use-package lsp-pyright
  :ensure t
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp))))

;; (maybe-require-package 'company-anaconda)
;; (eval-after-load "company"
;; (add-hook 'python-mode-hook 'anaconda-mode)
;; (add-hook 'python-mode-hook 'jedi:setup)
;;(add-hook 'python-mode-hook 'subword-mode)
;;       (append python-environment-virtualenv
;;               '("--python" "/usr/bin/python3")))

(setq org-src-window-setup 'other-window)
(setq org-src-preserve-indentation t)

;;(add-hook 'org-mode-hook 'auto-complete-mode)

(add-hook 'org-src-mode-hook
	  (lambda () (local-set-key (kbd "C-c C-e")
		       'org-src-do-key-sequence-at-code-block)))

(add-hook 'python-mode-hook 'hs-minor-mode)
(maybe-require-package 'circe)

(require 'man)
(define-key Man-mode-map (kbd "M-n") "\C-u4\C-v")
(define-key Man-mode-map (kbd "M-p") "\C-u4\M-v")

(maybe-require-package 'zotxt)
(eval-after-load "zotxt"
  '(setq zotxt-default-bibliography-style "mkbehr-short"))
(add-hook 'org-mode-hook 'org-zotxt-mode)

(maybe-require-package 'bash-completion)
(setq bash-completion-nospace t)
(defun bash-completion-eshell-capf ()
  (bash-completion-dynamic-complete-nocomint
   (save-excursion (eshell-bol) (point))
   (point) t))

(defun bash-completion-from-eshell ()
  (interactive)
  (let ((completion-at-point-functions
         '(bash-completion-eshell-capf)))
    (completion-at-point)))
;;WTF
(add-hook 'eshell-mode-hook (lambda () (define-key eshell-mode-map (kbd "C-i") 'bash-completion-from-eshell )))


(maybe-require-package 'flymake)
(add-hook 'LaTeX-mode-hook 'flymake-mode)
(use-package flymake
  :bind (:map flymake-mode-map
	      ("C-c C-," . 'flymake-goto-next-error)
	      ("C-c C-." . 'flymake-goto-prev-error))) 

(maybe-require-package 'auctex)
;; (require 'org-define-mode)
;; (add-hook 'org-mode-hook 'org-define-mode)
;; (require 'org-autobuild)
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq TeX-save-query nil)
(setq-default TeX-master nil)
(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

;;don't ask master
(setq-default TeX-master nil)

;; (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
;; (setq reftex-plug-into-AUCTeX t)
;; setup for reftex to handle refrences
;; (require 'tex)
(setq TeX-PDF-mode t)
(setq TeX-view-program-selection '((output-pdf "PDF Tools"))
      TeX-source-correlate-start-server t)

(add-hook 'TeX-after-compilation-finished-functions
          #'TeX-revert-document-buffer)

;;TODO use this to use make with auctex
;; (eval-after-load "tex" '(add-to-list 'TeX-command-list
;; 				     '("Make" "make" TeX-run-compile nil t)))
(setq latex-run-command "pdflatex")
(setq-default TeX-master 'shared)
;; I think uses stock tex instead of auctex

;;not sure what this does when true, except breaks preview, unless fix from reddit
;;also may want to use synctex? not sure what it does
;;also what the hell is latex-magic buffer

(maybe-require-package 'web-mode)
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html.tera?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsp?\\'" . web-mode))
(setq web-mode-engines-alist
      '(("django"    . "\\.html\\'")
	("django"    . "\\.tera\\'")
        ("jsp"       . "\\.jsp\\"))
      )
(use-package web-mode
  :bind (:map web-mode-map
	      ("C-c i" . 'web-mode-element-insert)))


;; doesn't exist anymore??
;; (maybe-require-package 'dired-x)
;; (add-hook 'dired-load-hook
;; 	  (function (lambda () (load "dired-x"))))


(add-hook 'dired-load-hook (lambda () (require 'dired-x)))
(setq dired-omit-mode t)

(maybe-require-package 'flyspell)
(maybe-require-package 'pdf-tools)
(pdf-loader-install)
(add-hook 'pdf-view-mode-hook (lambda () (linum-mode 0)))

(maybe-require-package 'keyfreq)
(keyfreq-mode 1)
(keyfreq-autosave-mode 1)

(eval-when-compile
  (require 'cl))

(setq helm-command-prefix-key "<C-return>")
(maybe-require-package 'helm-descbinds)
(maybe-require-package 'helm-dictionary)
(maybe-require-package 'helm)
(helm-mode 1)
(define-key helm-map (kbd "C-z")  'helm-select-action)
(define-key helm-find-files-map (kbd "C-<backspace>") 'backward-kill-word)
(define-key helm-read-file-map (kbd "C-<backspace>") 'backward-kill-word)
(define-key helm-find-files-map (kbd "<tab>") 'ignore ) 
(define-key helm-read-file-map (kbd "<tab>") 'ignore)
(helm-descbinds-mode)
(define-key helm-command-map (kbd "d") 'helm-dictionary)
(define-key helm-command-map (kbd "C-s") 'helm-occur)
(define-key helm-command-map (kbd "C-g") 'helm-grep-do-git-grep)

(define-key helm-grep-mode-map "\M-n" "\C-u4\C-v")
(define-key helm-grep-mode-map "\M-p" "\C-u4\M-v")
;;TODO integrate helm with helpful??

;;TODO URGENT re-enable
;; (maybe-require-package 'markdown-mode)
;; (add-hook 'markdown-mode-hook 'flyspell-mode)
;; (add-hook 'markdown-mode-hook 'visual-line-mode)
;; (define-key markdown-mode-map "\M-n" "\C-u4\C-v")
;; (define-key markdown-mode-map "\M-p" "\C-u4\M-v")

(defun markdown-html (buffer)
  (princ (with-current-buffer buffer
           (format "<!DOCTYPE html><html><title>Impatient Markdown</title><xmp theme=\"united\" style=\"display:none;\"> %s  </xmp><script src=\"http://ndossougbe.github.io/strapdown/dist/strapdown.js\"></script></html>" (buffer-substring-no-properties (point-min) (point-max))))
         (current-buffer)))

;; honestly not that useful
;; (require 'god-mode)
;; (global-set-key (kbd "<escape>") 'god-local-mode)
;; (define-key god-local-mode-map "i" 'god-local-mode)
;; (define-key god-local-mode-map "z" 'repeat)

(require 'org)

(maybe-require-package 'avy)
(define-key org-mode-map (kbd "C-j") 'avy-goto-word-or-subword-1)
(define-key lisp-interaction-mode-map (kbd "C-j") 'avy-goto-word-or-subword-1)
(define-key lisp-interaction-mode-map (kbd "C-M-j") 'eval-print-last-sexp)
(key-chord-define org-mode-map "qi" '(lambda () (interactive) (insert "\\")))
(define-key org-mode-map (kbd "H-o") 'helm-occur)
(define-key org-mode-map (kbd "H-l") 'rorg-double-link)

;; (pyvenv-activate "~/benv/benv")
;; (pyvenv-activate "~/everlaw/servers/.env")
;; (pyvenv-activate "/home/ryanwilson/everlaw/servers/pyutil/venv/nondocker/latest/everlaw")
;; (require 'dap-python)
;; (setq dap-python-debugger 'debugpy)
;; (pyvenv-activate "/home/ryan/Desktop/code/lwrote/venv")

;; (use-package projectile
;;   :ensure t
;;   :config
;;   (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
;;   (projectile-mode +1))

(require 'init-python)

(maybe-require-package 'paredit)
(autoload 'paredit-splice-sexp "paredit")

;; (maybe-require-package 'ob-clojure)
;; (setq org-babel-clojure-backend 'cider)
;; (maybe-require-package 'cider)
;; (add-hook 'clojure-mode-hook 'enable-paredit-mode)
;; (define-key paredit-mode-map (kbd "C-j") nil)
;; (define-key cider-mode-map (kbd "C-c C-e") nil)

;; (maybe-require-package 'lammps-mode)
;; (autoload 'lammps-mode "lammps-mode.el" "LAMMPS mode." t)
;; (setq auto-mode-alist (append auto-mode-alist
;;                               '(("in\\." . lammps-mode))
;;                               '(("\\.lmp\\'" . lammps-mode))
;;                               ))



(maybe-require-package 'hl-todo)
(global-hl-todo-mode 1)
(define-key hl-todo-mode-map (kbd "C-c C-h n") 'hl-todo-next)
(define-key hl-todo-mode-map (kbd "C-c C-h p") 'hl-todo-previous)

;; (maybe-require-package 'yafolding)
;; (define-key yafolding-mode-map (kbd "<C-return>") nil)
;; (define-key yafolding-mode-map (kbd "H-f") 'yafolding-toggle-element)
;; yafolding glitched out hard on me
;; (define-key hs-minor-mode-map (kbd "H-t") 'hs-toggle-hiding)

;; funny but not helpful
;; (require 'disable-mouse)
;; (global-disable-mouse-mode)


(set-fontset-font (frame-parameter nil 'font) 'han "xft:-GOOG-Noto Sans CJK KR-normal-normal-normal-*-16-*-*-*-*-0-iso10646-1")

;; (git-ensure-package "https://depp.brause.cc/nov.el.git" "nov.el")
;; (setq nov-unzip-program "unzzip") ;;TODO figure out how to unzip
;; (maybe-require-package 'nov)
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
;; (add-to-list 'load-path "~/.emacs.d/addedPackages/ibus-el-0.3.2")
;; (require 'ibus)

(maybe-require-package 'elfeed)
(require 'elfeed)
(define-key elfeed-search-mode-map (kbd "C-l") 'elfeed-update)
(define-key elfeed-search-mode-map (kbd "k") 'next-line)
(define-key elfeed-search-mode-map (kbd "l") 'previous-line)
;; (define-key elfeed-show-mode-map (kbd "<return>") 'shr-copy-url)
(define-key elfeed-show-mode-map (kbd "<return>") 'browse-url)

(maybe-require-package 'hungry-delete)
(global-hungry-delete-mode)

;;https://robert.kra.hn/posts/2021-02-07_rust-with-emacs/
;; to use dap with rust you need some extra stuff


;; (maybe-require-package 'tabbar)
;; (add-hook 'tabbar-mode-hook (lambda () (interactive) (remove-hook 'kill-buffer-hook 'tabbar-buffer-track-killed)))
;; (remove-hook 'kill-buffer-hook 'tabbar-buffer-track-killed)
;; (global-set-key (kbd "C-s-;") 'tabbar-forward-tab)
;; (global-set-key (kbd "C-s-j") 'tabbar-backward-tab)

(maybe-require-package 'cbm)
(global-set-key (kbd "C-s-;") #'cbm-cycle)

(maybe-require-package 'youdao-dictionary)

;; (add-to-list 'load-path "~/Desktop/code/newsr/")
;; (require 'newsr)
;; (define-key newsr-view-mode-map (kbd "d") 'youdao-dictionary-search-at-point)
;; (define-key newsr-view-mode-map (kbd "o") 'youdao-dictionary-search-at-point)
;; (define-key newsr-view-mode-map  (kbd "SPC") 'set-mark-command)
;; (define-key newsr-view-mode-map (kbd "j") 'backward-char)
;; (define-key newsr-view-mode-map (kbd ";") 'forward-char)
;; (define-key newsr-view-mode-map (kbd "k") 'next-line)
;; (define-key newsr-view-mode-map (kbd "l") 'previous-line)
;; (define-key newsr-view-mode-map (kbd "g") 'keyboard-quit)

;; (require 'general)

(yas-global-mode)
;; (define-key yas-minor-mode-map (kbd "C-c C-i ") 'yas-insert-snippet)

(add-hook 'diary-list-entries-hook 'diary-sort-entries)

(git-ensure-package "https://github.com/ryanswilson59/ob-wolfram" "ob-wolfram")

;; (maybe-require-package 'edbi)

;; (add-to-list 'load-path "/home/ryanwilson/.emacs.d/customModes/")
(add-to-list 'load-path (expand-file-name "custom-packages" user-emacs-directory))

(use-package typescript-mode
  :after tree-sitter
  :config
  ;; we choose this instead of tsx-mode so that eglot can automatically figure out language for server
  ;; see https://github.com/joaotavora/eglot/issues/624 and https://github.com/joaotavora/eglot#handling-quirky-servers
  ;; (define-derived-mode typescriptreact-mode typescript-mode
  ;;   "TypeScript TSX")

  ;; use our derived mode for tsx files
  ;; (add-to-list 'auto-mode-alist '("\\.tsx?\\'" . typescriptreact-mode))
  ;; by default, typescript-mode is mapped to the treesitter typescript parser
  ;; use our derived mode to map both .tsx AND .ts -> typescriptreact-mode -> treesitter tsx
  ;; (add-to-list 'tree-sitter-major-mode-language-alist '(typescriptreact-mode . tsx))
  )

(maybe-require-package 'coverlay)
(git-ensure-package "https://github.com/orzechowskid/tsi.el" "tsi")
(require 'origami)
(require 'tsx-mode)
(add-hook  'tsx-mode-hook #'lsp)
(add-hook  'typescript-mode-hook #'lsp)

(defun rsw-ts-offset ()
  (interactive)
  (progn
    (make-local-variable 'ts-indent-offset)
    (setq ts-indent-offset -1)))

(defun rsw-tab-func ()
  (interactive)
  (progn
    (setq tab-width 4)
    (setq indent-tabs-mode nil)))

(add-hook  'tsx-mode-hook #'rsw-tab-func)
(add-hook  'typescript-mode-hook #'rsw-tab-func)

(add-hook  'tsx-mode-hook #'run-col-3)
(add-hook  'java-mode-hook #'run-col-3)
(add-hook  'typescript-mode-hook #'run-col-3)

(setq tsi-typescript-indent-offset 4)
(define-derived-mode typescript-tsx-mode typescript-mode "TSX")
(define-key typescript-tsx-mode-map (kbd "C-c i") 'web-mode-element-insert)
(define-key tsx-mode-map (kbd "C-c i") 'web-mode-element-insert)

(defun rsw-enable-auto-save ()
  (interactive)
  (add-hook 'before-save-hook #'lsp-format-buffer))

(add-hook 'typescript-mode-hook #'lsp-headerline-breadcrumb-mode)
(setq lsp-headerline-breadcrumb-icons-enable t)
(setq lsp-headerline-breadcrumb-segments '(file symbols))
(setq lsp-ui-sideline-enable nil)
(setq lsp-ui-doc-enable nil)
(setq lsp-ui-doc-show-with-cursor nil)
(setq lsp-ui-doc-show-with-mouse nil)

(define-prefix-command 'lsp-my-map)
(global-set-key (kbd "C-c C-l") 'lsp-my-map)
(define-key python-mode-map (kbd "C-c C-l") 'lsp-my-map)

(define-key lsp-my-map (kbd "d") 'lsp-ui-doc-glance)
(define-key lsp-my-map (kbd "C-d") 'lsp-ui-doc-glance)
(define-key lsp-my-map (kbd "p") 'lsp-describe-thing-at-point)
(define-key lsp-my-map (kbd "C-p") 'lsp-describe-thing-at-point)
(define-key lsp-my-map (kbd "i") 'lsp-ui-peek-find-implementation)
(define-key lsp-my-map (kbd "C-i") 'lsp-ui-peek-find-implementation)
(define-key lsp-my-map (kbd "r") 'lsp-ui-peek-find-references)
(define-key lsp-my-map (kbd "C-r") 'lsp-ui-peek-find-references)
(define-key lsp-my-map (kbd "C-g") 'lsp-find-definition)
(define-key lsp-my-map (kbd ".") 'lsp-ui-peek-find-definitions)
(define-key lsp-my-map (kbd "C-.") 'lsp-ui-peek-find-definitions)
(define-key lsp-my-map (kbd "o") 'rsw-go-to-lspsym)

(git-ensure-package "https://github.com/jcs-elpa/codemetrics" "codemetrics")
(require 'codemetrics)
(setq codemetrics-rules
  `((c-mode          . ,(codemetrics-rules-c))
    (c++-mode        . ,(codemetrics-rules-c++))
    (csharp-mode     . ,(codemetrics-rules-csharp))
    (elixir-mode     . ,(codemetrics-rules-elixir))
    (emacs-lisp-mode . ,(codemetrics-rules-elisp))
    (go-mode         . ,(codemetrics-rules-go))
    (java-mode       . ,(codemetrics-rules-java))
    (javascript-mode . ,(codemetrics-rules-javascript))
    (js-mode         . ,(codemetrics-rules-javascript))
    (js2-mode        . ,(codemetrics-rules-javascript))
    (js3-mode        . ,(codemetrics-rules-javascript))
    (julia-mode      . ,(codemetrics-rules-julia))
    (lua-mode        . ,(codemetrics-rules-lua))
    (php-mode        . ,(codemetrics-rules-php))
    (python-mode     . ,(codemetrics-rules-python))
    (rjsx-mode       . ,(codemetrics-rules-javascript))
    (ruby-mode       . ,(codemetrics-rules-ruby))
    (rust-mode       . ,(codemetrics-rules-rust))
    (rustic-mode     . ,(codemetrics-rules-rust))
    (sh-mode         . ,(codemetrics-rules-bash))
    (scala-mode      . ,(codemetrics-rules-scala))
    (swift-mode      . ,(codemetrics-rules-swift))
    (tsx-mode        . ,(codemetrics-rules-typescript))
    (typescript-mode . ,(codemetrics-rules-typescript))))

(defun rsw-go-to-lspsym ()
  (interactive)
  (let* ((buffer (get-buffer "*LSP Symbols List*"))
         (window (get-buffer-window buffer t))
         (frame (window-frame window)))
    (select-frame-set-input-focus frame)
    (select-window window)))

(defun dedicate-this-window ()
  (interactive)
  (set-window-dedicated-p (selected-window) t))


(setq lsp-ui-doc-position 'at-point)

(use-package typescript-mode
  :bind (:map typescript-mode-map
	      ("C-." . lsp-find-definition)
	      ("M-." . lsp-find-references)))

(global-set-key (kbd "C-.") 'lsp-find-definition)
(global-set-key (kbd "M-.") 'lsp-find-references)

(define-key emacs-lisp-mode-map (kbd "M-.") 'xref-find-definitions)

(use-package tsx-mode
  :bind (:map tsx-mode-map
	      ("C-." . lsp-find-definition)
	      ("M-." . lsp-find-references)))
(add-to-list 'auto-mode-alist '("\\.tsx\\'" . tsx-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'" . javascript-mode))

;; (lsp-headerline-mode)
;; (defun dap-ui-controls-mode (&rest e) (interactive) ())
;; (tsx-mode 1)

(define-key python-mode-map (kbd "C-.") 'lsp-find-definition)
(define-key python-mode-map (kbd "M-.") 'lsp-find-references)

(require 'string-inflection)
(global-set-key (kbd "C-M-e") 'string-inflection-java-style-cycle)

(maybe-require-package 'visual-regexp-steroids)
(define-key global-map (kbd "C-c r") 'vr/replace)
(define-key global-map (kbd "C-c q") 'vr/query-replace)
(define-key global-map (kbd "C-M-%") 'vr/query-replace)
;; if you use multiple-cursors, this is for you:
(define-key global-map (kbd "C-c m") 'vr/mc-mark)
;; to use visual-regexp-steroids's isearch instead of the built-in regexp isearch, also include the following lines:
(define-key esc-map (kbd "C-r") 'vr/isearch-backward) ;; C-M-r
(define-key esc-map (kbd "C-s") 'vr/isearch-forward)

(maybe-require-package 'git-gutter-fringe)
(use-package git-gutter
  :hook (prog-mode . git-gutter-mode)
  :config
  (setq git-gutter:update-interval 0.1))

(use-package git-gutter-fringe
  :config
  (define-fringe-bitmap 'git-gutter-fr:added [224] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:modified [224] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:deleted [128 192 224 240] nil nil 'bottom))



;; TODO get this to run after mode load
(maybe-require-package 'diminish)

(progn
  (diminish 'eldoc-mode)
  (diminish 'racer-mode)
  (diminish 'disable-mouse-mode)
  (diminish 'disable-mouse-global-mode)
  (diminish 'projectile-mode)
  (diminish 'helm-mode)
  (diminish 'yas-minor-mode)
  (diminish 'which-key-mode)
  (diminish 'cargo-minor-mode)
  (diminish 'company-mode)
  (diminish 'auto-revert-mode)
  (diminish 'gcmh-mode)
  (diminish 'git-gutter-mode)
  (diminish 'editorconfig-mode)
  (diminish 'smerge-mode)
  (diminish 'tree-sitter-mode "tsm"))

(use-package avy
  :bind(( "C-;" . avy-goto-char-timer)
	( "C-j" . avy-goto-word-or-subword-1)
	( "M-j" . avy-goto-line)
	( "M-i" . avy-isearch)))
(global-set-key (kbd "H-k") 'avy-kill-region)
(global-set-key (kbd "H-y") 'avy-kill-ring-save-region)


(require 'avy)
(defun ravar/force-eldoc (&rest ignore) (interactive )
       (eldoc-print-current-symbol-info t))
(advice-add #'avy-goto-word-or-subword-1 :after #'ravar/force-eldoc)

(use-package ace-window
  :bind(( "M-o" . ace-window)))
(require 'ace-window)
(setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
(global-set-key (kbd "H-m") 'aw-show-dispatch-help)

(maybe-require-package 'magit)
(use-package magit
  :bind (:map magit-mode-map
              ( "C-t" . magit-section-cycle)))

(defun mu-magit-kill-buffers (param)
  "Restore window configuration and kill all Magit buffers."
  (let ((buffers (magit-mode-get-buffers)))
    (magit-restore-window-configuration)
    (mapc #'kill-buffer buffers)))

(defun show-file-name ()
  "Show the full path file name in the minibuffer."
  (interactive)
  (message (buffer-file-name))
  (kill-new (file-truename buffer-file-name))
)

(setq magit-bury-buffer-function #'mu-magit-kill-buffers)


;; TODO uncomment figure out what is going on with emacs 28
(use-package latex
  :bind (:map LaTeX-mode-map (("C-x C-s" . (lambda ()
			   "Save the buffer and run `TeX-command-run-all`."
			   (interactive)
			   (save-buffer)
			   (TeX-command-run-all nil)))
			      ("C-j" . avy-goto-word-or-subword-1)))
  :config (key-chord-define LaTeX-mode-map "qi" '(lambda () (interactive) (insert "\\")))
  :after (avy))

(use-package nov
  :bind (:map nov-mode-map
              ("C-d" . youdao-dictionary-search-at-point)
	      ("d" . danish-dictionary-at-point)
	      ("j" . backward-char)
	      (";" . forward-char)
	      ("k" . next-line)
	      ("l" . previous-line)
	      ("g" . keyboard-quit)
	      ("M-n" . scroll-down)
	      ("SPC" . scroll-up)
	      ("n" . "\C-u4\C-v")
	      ("p" . "\C-u4\M-v")
	      ;; ("SPC" . set-mark-command)
	      ("q" . nil )
	      ("C-q" . quit-window)
	      ("s" . nov-save))
  :config (nov-after-load-patch)
  :init
  (add-hook 'nov-post-html-render-hook 'nov-center-text-after-render))

;;   (add-hook 'nov-pre-html-render-hook 'my-nov-pre-ruby-hook)
;;   (add-hook 'nov-post-html-render-hook 'my-nov-shrink-rubies)
;; (remove-hook 'nov-post-html-render-hook 'nov-center-text-after-render)


(setq nov-variable-pitch nil)


(use-package flyspell-mode
  :bind (:map flyspell-mode-map
              ("C-c C-d" . ispell-word))
  :after (avy))

(use-package pdf-tools
  :bind (:map pdf-view-mode-map
              ("H" . pdf-annot-add-highlight-markup-annotation)
	      ("t" . pdf-annot-add-text-annotation)
	      ("d" . pdf-annot-delete)
	      ("M-SPC" . pdf-view-scroll-down-or-previous-page)))

(use-package circe
  :bind (:map circe-mode-map
              ("M-n" . "\C-u4\C-v")
	      ( "M-p" . "\C-u4\M-v")))


(maybe-require-package 'frog-jump-buffer)
(define-prefix-command 'my-lazy-map)
(global-set-key (kbd "C-M-m") 'my-lazy-map)
(define-key my-lazy-map (kbd "h") 'lsp-headerline-breadcrumb-mode)
(define-key my-lazy-map (kbd "C-M-m") 'frog-jump-buffer)

;;--------------------------------------------------------------------------------
;; global binds
;;--------------------------------------------------------------------------------
(global-set-key (kbd "H-d") 'hs-toggle-hiding)
;; had this previously set to delete the file, a profoundly stupid idea
(global-set-key (kbd "H-r") 'rename-this-file-and-buffer)
(global-set-key "\C-cz" 'show-file-name)

(global-set-key "\M-n" "\C-u4\C-v")
(global-set-key "\M-p" "\C-u4\M-v")

;; (maybe-require-package 'multiple-cursors)
;; (global-set-key (kbd "C-c m c") 'mc/edit-lines)
;; (global-set-key (kbd "<M-right>") 'mc/mark-next-like-this)
;; (global-set-key (kbd "<M-left>") 'mc/mark-previous-like-this)
;; TODO use this?

(global-set-key [f10] 'magit-status)
(global-set-key [f11] 'magit-blame)
;; (setq magit-blame-styles
;;       '((margin
;;          (margin-format . ("%-8.8H %-10.10a %-7.7A%f"))
;;          (margin-width . 28)
;;          (margin-face . magit-blame-margin)
;;          (margin-body-face . magit-blame-dimmed))))
;; (use-package blamer
;;   :ensure t
;;   :bind (("C-<f11>" . blamer-show-commit-info)
;;          ("<f11>" . blamer-mode)
;; 	 )
;;   :defer 20
;;   :custom
;;   (blamer-idle-time 0.3)
;;   (blamer-min-offset 70)
;;   :custom-face
;;   (blamer-face ((t :foreground "#7a88cf"
;;                    :background nil
;;                    :height 80
;;                    :italic t)))
;;   :config
;;   (global-blamer-mode 0))
(setq blamer-max-lines 40)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key [f1] 'ansi-term)

(global-set-key "\C-x\C-f" 'helm-find-files)
(global-set-key (kbd "C-x k") (lambda () (interactive) (kill-buffer nil)))
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-c C-s") 'helm-occur)
(global-set-key (kbd "H-i") 'helm-imenu)

(global-set-key (kbd "C-M-j") "\C-\M-u\C-\M-f")
(global-set-key (kbd "C-M-n") 'forward-list)
(global-set-key (kbd "M-s") 'paredit-splice-sexp)

(maybe-require-package 'bm)
(autoload 'bm-toggle   "bm" "Toggle bookmark in current buffer." t)
(autoload 'bm-next     "bm" "Goto bookmark."                     t)
(autoload 'bm-previous "bm" "Goto previous bookmark."            t)
(autoload 'bm-show-all "bm" "Show bookmarked lines in all buffers." t)

(global-set-key (kbd "C-c C-b C-t") 'bm-toggle)
(global-set-key (kbd "C-c C-b C-n") 'bm-next)
(global-set-key (kbd "C-c C-b C-p") 'bm-previous)
(global-set-key (kbd "C-c C-b C-a") 'bm-show-all)

(maybe-require-package 'caps-lock)
(global-set-key (kbd "M-c") 'caps-lock-mode)

(global-set-key (kbd "H-c") 'mc/edit-lines)
(global-set-key (kbd "H-a") 'ace-mc-add-single-cursor)

(global-set-key (kbd "H-l") 'lsp-rename)

;;--------------------------------------------------------------------------------
;; Custom functionality that have implemented
;;--------------------------------------------------------------------------------

(require 'ravar-custom)

(global-set-key (kbd "C-<f2>") 'multi-occur-in-this-mode)
(global-set-key [f5] 'revert-this-buffer)
(global-set-key (kbd "M-J") 'move-temp-del)
(require 'youdao-dictionary)
(define-key youdao-dictionary-mode-map (kbd "i") 'youdao-extract)
;; (global-set-key (kbd "H-c") 'collapse-head-whitespace)
(define-key org-mode-map (kbd "H-k") 'avy-jump-open)
;; (global-set-key (kbd "H-y" ) 'avy-yank-line)

(add-hook 'elfeed-show-mode-hook
	  (lambda ()
	    (define-key elfeed-show-mode-map (kbd "o") 'ravar/elfeed-play-enclosure-mpd)))

;;--------------------------------------------------------------------------------
;; Many default configurations are frustrating, loads some modified functions
;;--------------------------------------------------------------------------------
(require 'patches)

;;--------------------------------------------------------------------------------
;; Ensures a running emacsclient
;;--------------------------------------------------------------------------------
(add-hook 'after-init-hook
          (lambda ()
            (require 'server)
            (unless (server-running-p)
              (server-start))))

;;--------------------------------------------------------------------------------
;; Loads configuration from using the 'customize' interface
;;--------------------------------------------------------------------------------

;; (desktop-save-mode 1)
;; (setq desktop-path '("." "~/.emacs.d/"))

(add-to-list 'load-path "~/.emacs.d/local.el")
(require 'local)

;; doing this to only load the real custom when everything works, so it doesn't get nuked by
;;  bad init files
;; (require 'default-custom)
(setq custom-file (expand-file-name "diff-custom.el" user-emacs-directory))

(when (file-exists-p custom-file)
  (load custom-file))

(provide 'init)

(global-set-key (kbd "H-u") 'upcase-char)
(put 'upcase-region 'disabled nil)
(put 'set-goal-column 'disabled nil)
(add-to-list 'exec-path "/usr/local/bin/node")
